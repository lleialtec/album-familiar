/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice;

import org.jamgo.BackofficeApplication;
import org.jamgo.services.session.SessionContextImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.vaadin.spring.annotation.VaadinSessionScope;

@Configuration
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	org.jamgo.services.PackageMarker.class,
	org.jamgo.vaadin.PackageMarker.class,
	org.jamgo.layout.PackageMarker.class,
	cat.fecec.services.PackageMarker.class,
	cat.fecec.model.PackageMarker.class,
	cat.fecec.backoffice.AlbFamBackofficeApplication.class
})
@EntityScan(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	cat.fecec.model.PackageMarker.class,
	cat.fecec.backoffice.AlbFamBackofficeApplication.class
})
@EnableJpaRepositories(basePackageClasses = {
	org.jamgo.model.PackageMarker.class,
	cat.fecec.model.repository.PackageMarker.class,

})
public class AlbFamBackofficeApplication extends BackofficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlbFamBackofficeApplication.class, args);
	}

	@Bean
	@VaadinSessionScope
	@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
	public SessionContextImpl sessionContext() {
		return new SessionContextImpl();
	}
}
