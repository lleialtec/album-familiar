/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.api;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cat.fecec.model.Photo;
import cat.fecec.model.Photo.Side;
import cat.fecec.services.PhotoService;

@Controller
@RequestMapping("api/photo")
public class PhotoController {
	
	private static final Logger logger = LoggerFactory.getLogger(PhotoController.class);

	@Autowired
	private PhotoService photoService;
	
	@RequestMapping(path = "all", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Photo> getAll() {
		return this.photoService.getPhotos();
	}
	
	@RequestMapping(path = "{id_photo}/owner/{id_owner}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody byte[] get(HttpServletResponse response, @PathVariable("id_owner") String idOwner, @PathVariable("id_photo") String idPhoto) {
		try {
			byte[] content = PhotoService.getPhoto(idOwner, idPhoto, Side.ANV);
			response.getOutputStream().write(content);
			return content;
		} catch (Exception e) {
			PhotoController.logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	@RequestMapping(path = "{id_photo}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody byte[] getById(HttpServletResponse response, @PathVariable("id_photo") String idPhoto) {
		try {
			Photo photo = this.photoService.getPhoto(Long.valueOf(idPhoto));
			byte[] content = PhotoService.getPhoto(photo.getOwner().getId().toString(), idPhoto, Side.ANV);
			response.getOutputStream().write(content);
			return content;
		} catch (Exception e) {
			PhotoController.logger.error(e.getMessage(), e);
		}
		return null;
	}
}
