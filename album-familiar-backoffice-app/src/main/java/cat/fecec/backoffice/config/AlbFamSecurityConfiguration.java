/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.config;

import org.jamgo.config.SecurityConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class AlbFamSecurityConfiguration extends SecurityConfiguration {

	// @formatter:off

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors()
				.and()
			.authorizeRequests()
//				.antMatchers("/push/**").permitAll()
//				.antMatchers("/resources/**").permitAll()
				.antMatchers("/api/**").permitAll()
				.antMatchers("/ui/web/**").permitAll()
				.antMatchers("/ui/login/**").permitAll()
				.antMatchers("/form/**").permitAll()
				.antMatchers("/ui/APP/**").permitAll()
				.antMatchers("/ui/UIDL/**").permitAll()
				.antMatchers("/console/**").permitAll()
				.antMatchers("/VAADIN/**").permitAll()
				.anyRequest().authenticated()
				.and()
			.csrf().disable()
			.exceptionHandling()
				.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/ui/login"))
				.and()
			.headers()
				.frameOptions().disable()
				.and()
			.httpBasic();
	}

	// @formatter:on
	
	@Bean
	public GrantedAuthorityDefaults grantedAuthorityDefaults() {
	    return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
	}

}
