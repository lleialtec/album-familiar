/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice;

import java.util.LinkedHashMap;

import javax.annotation.PostConstruct;

import org.jamgo.layout.details.RoleDetailsLayout;
import org.jamgo.model.entity.Role;
import org.jamgo.model.entity.RoleImpl;
import org.jamgo.ui.layout.BackofficeApplicationDef;
import org.jamgo.ui.layout.crud.CrudLayoutDef;
import org.springframework.stereotype.Component;

import cat.fecec.backoffice.details.AlbumUserDetailsLayout;
import cat.fecec.backoffice.details.OwnerDetailsLayout;
import cat.fecec.backoffice.details.ThesaurusDetailsLayout;
import cat.fecec.backoffice.search.OwnerSearchLayout;
import cat.fecec.model.AlbumUser;
import cat.fecec.model.Owner;
import cat.fecec.model.Thesaurus;

@Component
public class AlbFamBackofficeApplicationDef extends BackofficeApplicationDef {

	@PostConstruct
	public void init() {
		this.initCrudLayoutDefs();
	}

	private void initCrudLayoutDefs() {
		// for roles management:
		// if (VaadinService.getCurrentRequest().isUserInRole(Role.ROLE_ADMIN))

		this.layoutDefs = new LinkedHashMap<>();

//		this.layoutDefs.put(PermissionsLayout.class, CustomLayoutDef.builder()
//				.setId("permission")
//				.setNameSupplier(() -> this.messageSource.getMessage("permissions.title"))
//				.setImageName("permission")
//				.setLayoutClass(PermissionsLayout.class)
//				.build());

		this.layoutDefs.put(AlbumUser.class, CrudLayoutDef.<AlbumUser> builder()
			.setId("user")
			.setNameSupplier(() -> this.messageSource.getMessage("table.users"))
//			.setImageName("user")
			.setEntityClass(AlbumUser.class)
			.columnDef().setId("username").setCaptionSupplier(() -> "User name").setValueProvider(obj -> obj.getUsername()).end()
			.columnDef().setId("enabled").setCaptionSupplier(() -> "Enabled").setValueProvider(obj -> obj.getEnabled()).end()
			.detailsLayoutClass(AlbumUserDetailsLayout.class)
			.build());

		this.layoutDefs.put(Role.class, CrudLayoutDef.<RoleImpl> builder()
			.setId("role")
			.setNameSupplier(() -> this.messageSource.getMessage("table.roles"))
//			.setImageName("role")
			.setEntityClass(RoleImpl.class)
			.columnDef().setId("rolename").setCaptionSupplier(() -> "Role Name").setValueProvider(obj -> obj.getRolename()).end()
			.detailsLayoutClass(RoleDetailsLayout.class)
			.build());

//		this.layoutDefs.put(Language.class, CrudLayoutDef.<Language> builder()
//			.setId("language")
//			.setNameSupplier(() -> this.messageSource.getMessage("table.languages"))
//			.setImageName("language")
//			.setEntityClass(Language.class)
//			.columnDef().setId("name").setCaptionSupplier(() -> "Name").setValueProvider(obj -> obj.getName()).end()
//			.columnDef().setId("languageCode").setCaptionSupplier(() -> "Language Code").setValueProvider(obj -> obj.getLanguageCode()).end()
//			.columnDef().setId("countryCode").setCaptionSupplier(() -> "Country Code").setValueProvider(obj -> obj.getCountryCode()).end()
//			.detailsLayoutClass(LanguageDetailsLayout.class)
//			.build());

//		this.layoutDefs.put(LocalizedMessage.class, CrudLayoutDef.<LocalizedMessage> builder()
//				.setNameSupplier(() -> this.messageSource.getMessage("table.localizedMessages"))
//				.setEntityClass(LocalizedMessage.class)
//				.columnDef().setId("key").setCaptionSupplier(() -> "Key").setOrderAsc().setValueProvider(obj -> obj.getKey()).end()
//				.columnDef().setId("language").setCaptionSupplier(() -> "Languaje").setOrderAsc().setValueProvider(obj -> obj.getLanguage()).end()
//				.columnDef().setId("country").setCaptionSupplier(() -> "Country").setOrderAsc().setValueProvider(obj -> obj.getCountry()).end()
//				.columnDef().setId("value").setCaptionSupplier(() -> "Value").setValueProvider(obj -> obj.getValue()).end()
//				.detailsLayoutClass(LocalizedMessageDetailsLayout.class)
//				.tableLayoutVerticalConfig()
//				.build());

//		this.layoutDefs.put(ModelDef.class, CrudLayoutDef.<ModelDef> builder()
//			.setNameSupplier(() -> this.messageSource.getMessage("table.modelDef"))
//			.setEntityClass(ModelDef.class)
//			.columnDef().setId("modelName").setCaptionSupplier(() -> "Model Name").setOrderAsc().setValueProvider(obj -> obj.getModelName().getDefaultText()).end()
//			.detailsLayoutClass(ModelDefDetailsLayout.class)
//			.tableLayoutVerticalConfig()
//			.setAddEnabled(false)
//			.setRemoveEnabled(false)
//			.build());

//		this.layoutDefs.put(Photo.class, CustomLayoutDef. builder()
//				.setId("event")
//				.setNameSupplier(() -> this.messageSource.getMessage("table.searchPhoto"))
//				.setLayoutClass(PhotoDetailsLayout.class)
//				.build());
		
		this.layoutDefs.put(Owner.class, CrudLayoutDef.<Owner> builder()
			.setId("owner")
			.setEntityClass(Owner.class)
			.setNameSupplier(() -> this.messageSource.getMessage("table.photos"))
			.columnDef().setId("name").setCaptionSupplier(() -> this.messageSource.getMessage("owner.name")).setValueProvider(obj -> obj.getName()).end()
			.columnDef().setId("surnames").setCaptionSupplier(() -> this.messageSource.getMessage("owner.surnames")).setValueProvider(obj -> obj.getSurnames()).end()
			.columnDef().setId("photos").setCaptionSupplier(() -> "").setValueProvider(obj -> obj.getPhotos().size() + " " + this.messageSource.getMessage("owner.photos")).end()
			.detailsLayoutClass(OwnerDetailsLayout.class)
			.setOpenOnStart(true)
			.searchLayoutClass(OwnerSearchLayout.class)
			.setSearchLayoutCloseable(true)
			.build());

		this.layoutDefs.put(Thesaurus.class, CrudLayoutDef.<Thesaurus> builder()
				.setId("event")
				.setNameSupplier(() -> this.messageSource.getMessage("table.thesaurus"))
				.setEntityClass(Thesaurus.class)
				.columnDef().setId("name").setCaptionSupplier(() -> "").setValueProvider(obj -> obj.getName()).end()
				.detailsLayoutClass(ThesaurusDetailsLayout.class)
				.build());
	}

}
