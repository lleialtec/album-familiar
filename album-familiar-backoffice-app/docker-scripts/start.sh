#!/bin/bash

docker-compose up --no-start
docker network connect bridge album-familiar-backoffice
docker network connect bridge album-familiar-backoffice-db
# db creation
docker exec album-familiar-backoffice-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create user lleialtat@'\''%'\'' identified by '\''ll314lt4t'\'';"' 
docker exec album-familiar-backoffice-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create database album_familiar default character set utf8 collate utf8_general_ci;"'
docker exec album-familiar-backoffice-db sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "grant all on album_familiar.* to lleialtat@'\''%'\'' identified by '\''ll314lt4t'\'';"' ;
docker-compose up -d

# create dns container to be able to resolve containers by container name instead of ip
sudo docker run --name dns -p 8353:53/udp -d -v /var/run/docker.sock:/docker.sock phensley/docker-dns --domain lleialtat.org


#create user lleialtat@'%' identified by 'll314lt4t';
#create database album_familiar default character set utf8 collate utf8_general_ci;
#grant all on album_familiar.* to lleialtat@'%' identified by 'll314lt4t';

# add contrab to backup db every night
sudo crontab -e

#0 3 * * * sudo docker exec -ti album-familiar-backoffice-db  mysqldump -u root -pll314lt4t album_familiar > /var/backups/mariadb/album-$(date +\%Y\%m\%d\%H\%M\%S).sql
0 3 * * * docker exec -i album-familiar-backoffice-db sh -c 'exec mysqldump -u root -pll314lt4t album_familiar > /var/backups/mariadb/album-$(date +\%Y\%m\%d\%H\%M\%S).sql'

# restores db
sudo docker exec -i album-familiar-backoffice-db  mysql -u root -pll314lt4t album_familiar < /var/backups/mariadb/album-20191120030001.sql
