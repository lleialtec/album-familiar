/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.io;

import java.util.function.Function;
import java.util.function.Supplier;

import org.jamgo.model.entity.Province;
import org.jamgo.model.entity.Region;
import org.jamgo.model.entity.Town;
import org.jamgo.services.ModelDefService;
import org.jamgo.services.NestedDataProvider;
import org.jamgo.ui.layout.ie.CrudCustomFieldsExportLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cat.fecec.model.Photo;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SchoolExportLayout extends CrudCustomFieldsExportLayout<Photo> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ModelDefService modelDefServices;

	@Override
	public void setExportableFields() {
		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("school.nif"), Photo::getAuthor));
		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("school.name"), Photo::getPhotoTitle));
		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("school.town"),
				school -> school.getTown() != null ? school.getTown().getName().getDefaultText() : ""));

		Function<Region, Object> regionProvinceDataProvider = new NestedDataProvider<Region,Province>(Region::getProvince, Province::getName);
		Function<Town, Object> townRegionDataProvider = new NestedDataProvider<Town,Region>(Town::getRegion, regionProvinceDataProvider);
		Function<Photo, Object> schoolTownDataProvider = new NestedDataProvider<Photo,Town>(Photo::getTown, townRegionDataProvider);

		this.addExportableColumn(new ExportableColumn(() -> this.messageSource.getMessage("school.province"), schoolTownDataProvider));
	}

	@Override
	public void applyFieldSelection() {
		// do nothing
	}

	@Override
	public void setTClass() {
		this.tClazz = Photo.class;
	}

	@Override
	public void setEntityManagerBeanName() {
		// do nothing
	}

	@Override
	public Supplier<String> getEntityNameSupplier() {
		return () -> this.messageSource.getMessage("school.title");
	}

 }
