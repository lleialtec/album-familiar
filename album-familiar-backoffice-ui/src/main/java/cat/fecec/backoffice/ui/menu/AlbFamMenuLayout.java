/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.ui.menu;

import org.jamgo.model.entity.Role;
import org.jamgo.ui.layout.menu.MenuGroup;
import org.jamgo.ui.layout.menu.MenuLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.server.VaadinRequest;

import cat.fecec.model.AlbumUser;
import cat.fecec.model.Owner;
import cat.fecec.model.Thesaurus;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AlbFamMenuLayout extends MenuLayout {

	private static final long serialVersionUID = 1L;

	@Override
	public MenuGroup getRootMenuGroup() {

		MenuGroup configurationLayoutDefsGroup = new MenuGroup();
		if (VaadinRequest.getCurrent().isUserInRole("role_super_admin")) {
			configurationLayoutDefsGroup.setCaption(this.messageSource.getMessage("title.main.configuration"));
			configurationLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(AlbumUser.class)));
			configurationLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(Role.class)));
//			configurationLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(PermissionsLayout.class)));
		}
//		crudLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(Language.class)));
//		configurationLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(LocalizedMessage.class)));
//		configurationLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(ModelDef.class)));
//		MenuGroup crudLayoutDefsGroup = new MenuGroup();
//		crudLayoutDefsGroup.setCaption(this.messageSource.getMessage("title.main.tables"));
//		crudLayoutDefsGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(Owner.class)));

		MenuGroup menuGroup = new MenuGroup();

		if (!configurationLayoutDefsGroup.getMenuItems().isEmpty()) {
			menuGroup.addMenuItem(configurationLayoutDefsGroup);
		}
//		if (!crudLayoutDefsGroup.getMenuItems().isEmpty()) {
//			menuGroup.addMenuItem(crudLayoutDefsGroup);
//		}
		menuGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(Owner.class)));
		
		if (VaadinRequest.getCurrent().isUserInRole("role_super_admin")) {
			menuGroup.addMenuItem(this.createMenuItem(this.backofficeApplicationDef.getLayoutDef(Thesaurus.class)));
		}
		return menuGroup;
	}
}
