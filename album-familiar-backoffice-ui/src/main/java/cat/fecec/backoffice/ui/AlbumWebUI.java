/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.StyleSheet;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import cat.fecec.model.Photo;
import cat.fecec.model.Photo.Side;
import cat.fecec.model.repository.search.PhotoSearch;
import cat.fecec.services.PhotoService;

@SpringUI(path = "/web")
@Theme("fececTheme")
@Widgetset("cat.fecec.backoffice.Widgetset")
@StyleSheet({ "http://fonts.googleapis.com/css?family=Roboto:400,100,300,700" })
public class AlbumWebUI extends UI {

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(AlbumWebUI.class);
	
	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	private PhotoService photoService;
	@Autowired
	private LocalizedMessageService messageService;
	
	private static int PAGE_LENGTH = 8;

	private VerticalLayout contentLayout = new VerticalLayout();
	
	private GridLayout imageGrid;
	private Label emptyLabel;
	
	private List<Photo> photos;
	private List<Button> images = new ArrayList<Button>();
	private int currentPage = 0;
	
	public AlbumWebUI() {
		super();
		this.contentLayout.setSizeFull();
		this.contentLayout.setMargin(true);
		this.contentLayout.setSpacing(true);
	}
	
	@PostConstruct
	public void init() {
		Label titleLabel = new Label("<h1><b>L'ÀLBUM FAMILIAR DEL BARRI</b></h2>", ContentMode.HTML);
		titleLabel.addStyleName("font-green");
		titleLabel.setHeight(20, Unit.PIXELS);
		this.contentLayout.addComponent(titleLabel);
		this.contentLayout.setComponentAlignment(titleLabel, Alignment.TOP_CENTER);
		
		Label subtitleHeader = new Label("<h2><b>Un àlbum col.lectiu de Sants i les seves gents</b></h2>", ContentMode.HTML);
		subtitleHeader.addStyleName("font-grey");
		titleLabel.setHeight(20, Unit.PIXELS);
		this.contentLayout.addComponent(subtitleHeader);
		this.contentLayout.setComponentAlignment(subtitleHeader, Alignment.TOP_CENTER);
		
		TextField searchField = new TextField("");
		searchField.setWidth(310*4,Unit.PIXELS);
		searchField.setPlaceholder("Escriu aqui el text per cercar...");
		
		Button searchButton = new Button(VaadinIcons.SEARCH.getHtml());
		searchButton.setCaptionAsHtml(true);
		searchButton.addClickListener(listener -> {
			PhotoSearch photoSearch = new PhotoSearch();
			photoSearch.setText(searchField.getValue());
			List<Photo> photos = this.photoService.search(photoSearch);
			this.photos = photos;
			this.refreshPicturesGrid();
		});

		this.emptyLabel = new Label("<h1><b>No hi ha fotografies</b></h2>", ContentMode.HTML);
		
		HorizontalLayout searchLayout = new HorizontalLayout(searchField, searchButton);
		searchLayout.setExpandRatio(searchField, 1);
		searchLayout.setComponentAlignment(searchField, Alignment.BOTTOM_CENTER);
		searchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_CENTER);
		this.contentLayout.addComponent(searchLayout);
		this.contentLayout.setComponentAlignment(searchLayout, Alignment.MIDDLE_CENTER);

		HorizontalLayout imagesLayout = new HorizontalLayout();
		this.imageGrid = new GridLayout(4,2);
		this.imageGrid.setWidth(310*4,Unit.PIXELS);
		this.imageGrid.setMargin(true);
		this.imageGrid.setSpacing(true);
		Button nextButton = new Button();
		nextButton.setIcon(new ThemeResource("images/right-arrow.png"));
		nextButton.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		nextButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		nextButton.addStyleName("arrow-button");
		nextButton.addClickListener(listener -> {
			if (this.currentPage < ((this.photos.size()/AlbumWebUI.PAGE_LENGTH) - 1)) {
				this.currentPage++;
			} else {
				this.currentPage = 0;
			}
			this.refreshPicturesGrid();
		});
		Button previousButton = new Button();
		previousButton.setIcon(new ThemeResource("images/left-arrow.png"));
		previousButton.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		previousButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		previousButton.addStyleName("arrow-button");
		previousButton.addClickListener(listener -> {
			if (this.currentPage > 0) {
				this.currentPage--;
			} else {
				this.currentPage = (this.photos.size()/AlbumWebUI.PAGE_LENGTH) -1;
			}
			this.refreshPicturesGrid();
		});
		imagesLayout.addComponents(previousButton, this.imageGrid, nextButton);
		imagesLayout.setExpandRatio(this.imageGrid, 1);
		imagesLayout.setComponentAlignment(previousButton, Alignment.MIDDLE_RIGHT);
		imagesLayout.setComponentAlignment(this.imageGrid, Alignment.MIDDLE_CENTER);
		imagesLayout.setComponentAlignment(nextButton, Alignment.MIDDLE_LEFT);
		imagesLayout.setWidth(80, Unit.PERCENTAGE);
		
		this.contentLayout.addComponent(imagesLayout);
		this.contentLayout.setExpandRatio(imagesLayout, 1);
		this.contentLayout.setComponentAlignment(imagesLayout, Alignment.MIDDLE_CENTER);
		
		this.setContent(this.contentLayout);
	}

	@Override
	public void attach() {
		super.attach();
		this.renameFotos();
		this.photos = this.photoService.getPhotos().stream().filter(photo -> {
			File file = PhotoService.getRegistryFile(photo.getOwner().getId().toString(), photo.getId().toString(), Side.ANV.name(), PhotoService.JPG_EXTENSION);
			return file.exists();
		}).collect(Collectors.toList());
		this.refreshPicturesGrid();
	}

	@Override
	protected void init(VaadinRequest request) {
		// ...	We don't need a Navigator in thes UI. If Navigator is not null, default initialization
		//		fires an exception.
		this.setNavigator(null);
		
		Page.getCurrent().getStyles().add(".v-layout {background-color : white;}");
		Page.getCurrent().getStyles().add(".image-hover:hover {opacity : 0.5;}");
		Page.getCurrent().getStyles().add(".font-green h1 {color : #4b9895; font-weight: bold;}");
		Page.getCurrent().getStyles().add(".font-grey h2 {color : #a5a19d;}");
		Page.getCurrent().getStyles().add(".v-font-family: Roboto, sans-serif");
		Page.getCurrent().getStyles().add(".image-button img {width: 300px; height: 300px;}");
		Page.getCurrent().getStyles().add(".arrow-button img {width: 100px; height: 100px;}");
	}
	
	private void refreshPicturesGrid() {
		this.imageGrid.removeAllComponents();
		this.showNextImages();
	}
	
	private void showEmptyImages() {
		this.imageGrid.removeAllComponents();
		this.imageGrid.addComponent(this.emptyLabel, 0, 0, 3, 1);
		this.imageGrid.setComponentAlignment(this.emptyLabel, Alignment.MIDDLE_CENTER);
	}
	
	private void showNextImages() {
		if (this.photos.isEmpty()) {
			this.showEmptyImages();
		}
		int beginPosition = this.currentPage * AlbumWebUI.PAGE_LENGTH;
		int endPosition = beginPosition + AlbumWebUI.PAGE_LENGTH;
		endPosition = endPosition <= this.photos.size() ? endPosition : this.photos.size();
		for(int i = beginPosition; i < endPosition; i++) {
			try {
				Photo photo = this.photos.get(i);
				Button imageButton = null;
				if ((this.images.size() > i) && (this.images.get(i) != null)) {
					imageButton = this.images.get(i);
				} else {
					byte[] content = PhotoService.getPhoto(String.valueOf(photo.getOwner().getId()), String.valueOf(photo.getId()), Photo.Side.ANV);
					if (content != null) {
						imageButton = new Button();
						imageButton.addStyleName(ValoTheme.BUTTON_LINK);
						imageButton.addStyleName("image-hover");
						imageButton.addStyleName("image-button");
						StreamResource imageResource = new StreamResource(new StreamSource() {
							private static final long serialVersionUID = 1L;
							
							@Override
							public InputStream getStream() {
								return new ByteArrayInputStream(content);
							}
						}, photo.getId() + ".jpg");
						imageButton.setIcon(imageResource);
						imageButton.setData(photo);
						imageButton.setHeight(300, Unit.PIXELS);
						imageButton.setWidth(300, Unit.PIXELS);
						imageButton.addClickListener(listener -> {
							GridLayout photoLayout = new GridLayout(2,1);
							photoLayout.setSizeFull();
							photoLayout.setMargin(true);
							photoLayout.setSpacing(true);
							
							Image image = new Image();
							image.setSource(new StreamResource(new StreamSource() {
								private static final long serialVersionUID = 1L;
								
								@Override
								public InputStream getStream() {
									return new ByteArrayInputStream(content);
								}
							}, System.currentTimeMillis() + ".jpg"));
							image.setHeight(100, Unit.PERCENTAGE);
							photoLayout.addComponent(image, 0,0);
							photoLayout.setComponentAlignment(image, Alignment.MIDDLE_CENTER);
							photoLayout.setColumnExpandRatio(0, 1);
							
//							GridLayout photoInfoLayout = new GridLayout(2,2);
							VerticalLayout photoInfoLayout = new VerticalLayout();
							photoInfoLayout.setMargin(true);
							photoInfoLayout.setSpacing(true);
							Label dateLabel = new Label("<b>" + this.messageService.getMessage("photo.date") + "</b>", ContentMode.HTML);
							photoInfoLayout.addComponent(dateLabel);
							photoInfoLayout.setComponentAlignment(dateLabel, Alignment.MIDDLE_RIGHT);
							photoInfoLayout.addComponent(new Label(photo.getDate().toString()));
//							Label descriptionLabel = new Label("<b>" + this.messageService.getMessage("photo.description") + "</b>", ContentMode.HTML);
//							photoInfoLayout.addComponent(descriptionLabel);
//							photoInfoLayout.setComponentAlignment(descriptionLabel, Alignment.MIDDLE_RIGHT);
//							photoInfoLayout.addComponent(new Label(photo.getDescriptionAuthor()));
							Label titleLabel = new Label("<b>" + this.messageService.getMessage("photo.title") + "</b>", ContentMode.HTML);
							photoInfoLayout.addComponent(titleLabel);
							photoInfoLayout.setComponentAlignment(titleLabel, Alignment.MIDDLE_RIGHT);
							photoInfoLayout.addComponent(new Label(photo.getPhotoTitle()));
							Label placeLabel = new Label("<b>" + this.messageService.getMessage("photo.place") + "</b>", ContentMode.HTML);
							photoInfoLayout.addComponent(placeLabel);
							photoInfoLayout.setComponentAlignment(placeLabel, Alignment.MIDDLE_RIGHT);
							photoInfoLayout.addComponent(new Label(photo.getPlace()));
							photoLayout.addComponent(photoInfoLayout,1,0);
							photoLayout.setComponentAlignment(photoInfoLayout, Alignment.MIDDLE_CENTER);
							
							Window window = new Window();
							window.setModal(true);
							window.center();
							window.setResizable(false);
							window.setContent(photoLayout);
							window.setWidth(80, Unit.PERCENTAGE);
							window.setHeight(80, Unit.PERCENTAGE);
							window.addListener(list -> {
								System.out.println();
							});
							UI.getCurrent().addWindow(window);;
						});
					}
				}
				if (imageButton != null) {
					this.images.add(imageButton);
					this.imageGrid.addComponent(imageButton);
				}
			} catch (FileNotFoundException e) {
				AlbumWebUI.logger.error(e.getMessage(), e);
			}
		}
	}
	
	public void renameFotos() {
//		File file = new File("/opt/album.content");
//		for(File dir : file.listFiles()) {
//			for(File image : dir.listFiles(image -> image.getName().endsWith(".jpg"))) {
//				String imageName = image.getName().split("\\.")[0];
//				String[] imageNameParts = imageName.split("-");
//				if (imageNameParts.length == 3) {
//					boolean done = image.renameTo(new File(dir.getPath() + "/" + imageNameParts[0] + "-" + imageNameParts[1] + ".jpg"));
//					System.out.println(image.getName() + " " + done);
//					System.out.println(done ? "done" : "...");
//				}
//			}
//		}
	}
}
