/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.ui;

import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.ui.LoginUI;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

@SpringUI(path = "/login")
@Theme("fececTheme")
@Widgetset("cat.fecec.backoffice.Widgetset")
public class AlbumLoginUI extends LoginUI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private LocalizedMessageService messageService;

	@Override
	protected void navigateToView() {
		this.getPage().setLocation("/ui");
	}

	@Override
	protected HorizontalLayout createLoginHeaderLayout() {
		HorizontalLayout topBar = new HorizontalLayout();
		topBar.setWidth(100, Unit.PERCENTAGE);
		topBar.setHeight(100, Unit.PERCENTAGE);
		topBar.setStyleName("login-header");

		ThemeResource logoResource = new ThemeResource("images/logo-album.png");
		Image logo = new Image(null, logoResource);
		logo.setWidth(400, Unit.PIXELS);
		logo.setStyleName("login-header-logo");
		Label applicationTitle = new Label(this.messageService.getMessage("application.title"), ContentMode.HTML);
		applicationTitle.addStyleName("login-header-title");
		topBar.addComponents(logo, applicationTitle);
		topBar.setExpandRatio(logo, 1.0f);
		topBar.setExpandRatio(applicationTitle, 2.0f);
		topBar.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
		topBar.setComponentAlignment(applicationTitle, Alignment.BOTTOM_CENTER);

		return topBar;
	}

}
