/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.details;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;

import org.jamgo.model.entity.BinaryObject;
import org.jamgo.model.entity.BinaryObjectImpl;
import org.jamgo.model.entity.BinaryResource;
import org.jamgo.services.message.LocalizedMessageService;
import org.jamgo.vaadin.JamgoComponentBuilderFactory;
import org.jamgo.vaadin.ui.BinaryResourceTransferField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LMarker;
import org.vaadin.addon.leaflet.LOpenStreetMapLayer;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.addon.leaflet.shared.Point;
import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import cat.fecec.model.Photo;
import cat.fecec.model.Photo.Color;
import cat.fecec.model.Photo.Side;
import cat.fecec.services.ImageService;
import cat.fecec.services.PhotoService;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PhotoDetailsLayout extends GridLayout{

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(PhotoDetailsLayout.class); 
	
	@Autowired
	private JamgoComponentBuilderFactory componentBuilderFactory;
	@Autowired
	protected LocalizedMessageService messageSource;
	@Autowired
	protected PhotoService photoService;
	
	private Photo photo;
	private LMap map;
	private Binder<Photo> photoBinder;
	private Label registryNumber;
	
	private BinaryResourceTransferField backPicture;
	private BinaryResourceTransferField frontPicture;
	private Audio audio;
	private LMarker marker = new LMarker();
	
	public PhotoDetailsLayout() {
		super(2,7);
	}
	
	@PostConstruct
	public void init() {
		this.addDeleteButton();
		this.addPhotos();
		this.addInfo();
		this.addMap();
	}
	
	private void addDeleteButton() {
		Button deletePhotoButton = new Button(VaadinIcons.TRASH.getHtml());
		deletePhotoButton.setCaptionAsHtml(true);
		deletePhotoButton.addStyleName(ValoTheme.BUTTON_LINK);
		deletePhotoButton.addClickListener(listener -> {
			ConfirmDialog.show(UI.getCurrent(), "Esborrar foto", "Estàs segur de que vols eliminar aquesta foto?", "Acceptar", "Cancel·lar", click -> {
				if (click.isConfirmed()) {
					this.photoService.deletePhoto(this.photo);
					((TabSheet)this.getParent()).removeComponent(this);
				}
			});
		});
		this.addComponent(deletePhotoButton, 1, 0);
		this.setComponentAlignment(deletePhotoButton, Alignment.TOP_RIGHT);
	}
	
	private void addPhotos() {
		HorizontalLayout picturesLayout = new HorizontalLayout();
		
		this.photoBinder = new Binder<>(Photo.class);
		
		this.setWidth(100, Unit.PERCENTAGE);

		this.frontPicture = this.componentBuilderFactory.createBinaryResourceTransferFieldBuilder().setCaption("photo.front").build();
		this.frontPicture.setPreviewEnabled(true);
		this.frontPicture.setDescriptionEnabled(false);
		this.frontPicture.setDateEnabled(false);
		this.frontPicture.setFileNameEnabled(false);
		this.frontPicture.setHeight(100, Unit.PIXELS);
		this.frontPicture.setDeleteEnabled(false);
		this.frontPicture.setValue(null);
		this.frontPicture.setSucceededListener(listener -> {
			byte[] tiffContent = listener.getBinaryObject().getContents();
			this.photo.getTiffContents().put(Side.ANV, tiffContent);

			InputStream is = null;
			try {
				is = new ByteArrayInputStream(tiffContent);
				BinaryResource binaryResource = this.frontPicture.getValue();
				byte[] jpgImage = ImageService.convertoToJpg(is, null);
				this.photo.getJpgContents().put(Side.ANV, jpgImage);
//				jpgImage = ImageService.addReferenceMark(jpgImage, "Album familiar - Lleialtat Santsenca");
				binaryResource.setContents(jpgImage);
				binaryResource.setMimeType("image/jpeg");
				binaryResource.setFileLength(jpgImage.length);
				this.frontPicture.setValue(binaryResource);
				this.frontPicture.setHeight(400, Unit.PIXELS);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						PhotoDetailsLayout.logger.error(e.getMessage(), e);
					}
				}
			}
			
		});
		
		this.backPicture = this.componentBuilderFactory.createBinaryResourceTransferFieldBuilder().setCaption("photo.back").build();
		this.backPicture.setPreviewEnabled(true);
		this.backPicture.setDescriptionEnabled(false);
		this.backPicture.setDateEnabled(false);
		this.backPicture.setFileNameEnabled(false);
		this.backPicture.setHeight(100, Unit.PIXELS);
		this.backPicture.setDeleteEnabled(false);
		this.backPicture.setValue(null);
		this.backPicture.setSucceededListener(listener -> {
			byte[] tiffContent = listener.getBinaryObject().getContents();
			this.photo.getTiffContents().put(Side.REV, tiffContent);
			
			InputStream is = null;
			try {
				is = new ByteArrayInputStream(tiffContent);
				BinaryResource binaryResource = this.backPicture.getValue();
				ProgressBar progressBar = new ProgressBar();
				this.addComponent(progressBar);
				byte[] jpgImage = ImageService.convertoToJpg(is, x -> progressBar.setValue(x));
				this.removeComponent(progressBar);
				this.photo.getJpgContents().put(Side.REV, jpgImage);
				binaryResource.setContents(jpgImage);
				binaryResource.setMimeType("image/jpeg");
				binaryResource.setFileLength(jpgImage.length);
				this.backPicture.setValue(binaryResource);
				this.backPicture.setHeight(400, Unit.PIXELS);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						PhotoDetailsLayout.logger.error(e.getMessage(), e);
					}
				}
			}
		});

		picturesLayout.addComponents(this.frontPicture, this.backPicture);
		this.addComponent(picturesLayout, 0, 1, 1, 1);
		this.setComponentAlignment(picturesLayout, Alignment.MIDDLE_CENTER);
	}
	
	private void addInfo() {
		FormLayout photoFormLayout = new FormLayout();
		this.audio = new Audio("");
		this.addComponent(this.audio, 0,2,1,2);
		this.setComponentAlignment(this.audio, Alignment.MIDDLE_CENTER);
		
		BinaryResourceTransferField audioUploadField = this.componentBuilderFactory.createBinaryResourceTransferFieldBuilder().build();
		audioUploadField.setPreviewEnabled(false);
		audioUploadField.setDescriptionEnabled(false);
		audioUploadField.setDateEnabled(false);
		audioUploadField.setFileNameEnabled(false);
		audioUploadField.setDeleteEnabled(false);
		audioUploadField.setSucceededListener(listener -> {
			byte[] content = listener.getBinaryObject().getContents();
			this.photo.setAudioContent(content);
			// file name must be different in order to refresh audio on player
			this.audio.setSource(new StreamResource(() -> new ByteArrayInputStream(content), System.currentTimeMillis() + "-" + listener.getBinaryObject().getFileName()));
		});
		this.addComponent(audioUploadField, 0,3,1,3);
		this.setComponentAlignment(audioUploadField, Alignment.MIDDLE_CENTER);
		
		
		photoFormLayout = new FormLayout();
		photoFormLayout.setWidth(50, Unit.PERCENTAGE);
		this.registryNumber = new Label();
		this.registryNumber.setCaption(this.messageSource.getMessage("photo.registryNumber"));
		photoFormLayout.addComponent(this.registryNumber);

		TextField authorField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.author")
				.setWidth(100, Unit.PERCENTAGE).build();
		authorField.setWidth(10, Unit.EM);
		photoFormLayout.addComponent(authorField);
		this.photoBinder.bind(authorField, Photo::getAuthor, Photo::setAuthor);

		TextField photoTitleField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.title")
				.setWidth(100, Unit.PERCENTAGE).build();
		photoTitleField.setWidth(100, Unit.PERCENTAGE);
		photoFormLayout.addComponent(photoTitleField);
		this.photoBinder.bind(photoTitleField, Photo::getPhotoTitle, Photo::setPhotoTitle);

		TextArea annotationsField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("photo.annotations")
				.setWidth(100, Unit.PERCENTAGE).build();
		annotationsField.setMaxLength(5000);
		photoFormLayout.addComponent(annotationsField);
		this.photoBinder.bind(annotationsField, Photo::getAnnotations, Photo::setAnnotations);

		TextArea decriptorsField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("photo.descriptors")
				.setWidth(100, Unit.PERCENTAGE).build();
		decriptorsField.setMaxLength(5000);
		photoFormLayout.addComponent(decriptorsField);
		this.photoBinder.bind(decriptorsField, Photo::getDescriptors, Photo::setDescriptors);

		TextField placeField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.place").setWidth(100, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(placeField);
		this.photoBinder.bind(placeField, Photo::getPlace, Photo::setPlace);

		TextField dateField = this.componentBuilderFactory.createTextFieldBuilder().setWidth(100, Unit.PERCENTAGE).setCaption("photo.date")
				.setWidth(50, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(dateField);
		this.photoBinder.bind(dateField, Photo::getDate, Photo::setDate);

		TextField descriptionAuthorField = this.componentBuilderFactory.createTextFieldBuilder().setWidth(100, Unit.PERCENTAGE).setCaption("photo.decriptionAuthor")
				.setWidth(50, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(descriptionAuthorField);
		this.photoBinder.bind(descriptionAuthorField, Photo::getDescriptionAuthor, Photo::setDescriptionAuthor);
		
		DateField descriptionDateField = new DateField(this.messageSource.getMessage("photo.decriptionDate"));
		photoFormLayout.addComponent(descriptionDateField);
		this.photoBinder.forField(descriptionDateField)
						.withConverter(new LocalDateToDateConverter())
						.bind(Photo::getDescriptionDate, Photo::setDescriptionDate);
		
		TextField technicalDescriptionField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.technic")
				.setWidth(100, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(technicalDescriptionField);
		this.photoBinder.bind(technicalDescriptionField, Photo::getTechnicalDescription, Photo::setTechnicalDescription);
		
		TextField sizeField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.size")
				.setWidth(100, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(sizeField);
		this.photoBinder.bind(sizeField, Photo::getSize, Photo::setSize);
		
		RadioButtonGroup<Photo.Color> colorField = new RadioButtonGroup<>(this.messageSource.getMessage("photo.color"));
		colorField.setItems(Color.values());
		colorField.setItemCaptionGenerator(item -> this.messageSource.getMessage("photo.color." + item.name().toLowerCase()));
		photoFormLayout.addComponent(colorField);
		this.photoBinder.bind(colorField, Photo::getColor, Photo::setColor);
		
		TextField givenByField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.givenBy")
				.setWidth(100, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(givenByField);
		this.photoBinder.bind(givenByField, Photo::getGivedBy, Photo::setGivedBy);
				
		TextField sourceField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.source").setWidth(100, Unit.PERCENTAGE).setWidth(100, Unit.PERCENTAGE).build();
		photoFormLayout.addComponent(sourceField);
		this.photoBinder.bind(sourceField, Photo::getSource, Photo::setSource);
		
		TextArea otherInfoField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("photo.otherInfo")
				.setWidth(100, Unit.PERCENTAGE).build();
		otherInfoField.setMaxLength(5000);
		photoFormLayout.addComponent(otherInfoField);
		this.photoBinder.bind(otherInfoField, Photo::getOtherInformation, Photo::setOtherInformation);
		
		this.addComponent(photoFormLayout, 0,4,1,4);
		this.setComponentAlignment(photoFormLayout, Alignment.MIDDLE_CENTER);
	}
	
	private void addMap() {
		this.map = new LMap();
	    LTileLayer osmTiles = new LOpenStreetMapLayer();
        osmTiles.setAttributionString("© OpenStreetMap Contributors");
        
	    this.map.addBaseLayer(osmTiles, "OSM");
	    this.map.setCenter(41.373067, 2.136622);
		this.map.setWidth(80, Unit.PERCENTAGE);
		this.map.setHeight(400, Unit.PIXELS);
		this.map.addClickListener(click -> {
			Point point = click.getPoint();
			this.marker.setPoint(point);
			if (!this.map.hasComponent(this.marker)) {
				this.map.addLayer(this.marker);
			}
			
			this.photo.setLatitude(point.getLat());
			this.photo.setLongitude(point.getLon());
		});
		
        this.addComponent(this.map,0,5,1,5);
        this.setComponentAlignment(this.map, Alignment.MIDDLE_CENTER);
        
        Button clearMapButton = this.componentBuilderFactory.createButtonBuilder().setCaption("photo.map.clear").build();
        clearMapButton.addClickListener(listener -> {
        	this.map.removeComponent(this.marker);
        	this.photo.setLatitude(null);
        	this.photo.setLongitude(null);
        });
        clearMapButton.addStyleName(ValoTheme.BUTTON_LINK);
        this.addComponent(clearMapButton,0,6);
	}
	
	public void setPhoto(Photo photo) {
		this.photo = photo;
		this.photoBinder.setBean(this.photo);
		
		if (this.photo.getId() != null) {
			try {
				byte[] pictureAnv = PhotoService.getPhoto(this.photo.getOwner().getId().toString(), this.photo.getId().toString(), Side.ANV);
				byte[] pictureRev = PhotoService.getPhoto(this.photo.getOwner().getId().toString(), this.photo.getId().toString(), Side.REV);
				BinaryObject frontBinaryObject = new BinaryObjectImpl();
				if (pictureAnv != null) {
					frontBinaryObject.setFileName(Photo.Side.ANV + ".tiff");
					frontBinaryObject.setContents(pictureAnv);
					frontBinaryObject.setMimeType("image/jpeg");
//					// TODO set height or width depending on picture size format
					this.frontPicture.setHeight(400, Unit.PIXELS);
					this.frontPicture.setValue(frontBinaryObject);
				}
				
				BinaryObject backBinaryObject = new BinaryObjectImpl();
				if (pictureRev != null) {
					backBinaryObject.setFileName(Photo.Side.REV + ".tiff");
					backBinaryObject.setContents(pictureRev);
					backBinaryObject.setMimeType("image/jpeg");
//					// TODO set height or width depending on picture size format
					this.backPicture.setHeight(400, Unit.PIXELS);
					this.backPicture.setValue(backBinaryObject);
				}

				InputStream audioStream = PhotoService.getAudio(String.valueOf(this.photo.getOwner().getId()), String.valueOf(this.photo.getId()));
				if (audioStream != null) {
					this.audio.setSource(new StreamResource(() -> audioStream, this.photo.getId()  + ".wav"));
				}
			} catch (FileNotFoundException e) {
				PhotoDetailsLayout.logger.error(e.getMessage(), e);
			}
			
			if ((photo.getLatitude() != null) && (photo.getLongitude() != null)) {
				Point point = new Point(photo.getLatitude(), photo.getLongitude());
				this.marker.setPoint(point);
				this.map.addLayer(this.marker);
			    this.map.setCenter(point.getLat(), point.getLon());
			}
		}
		this.registryNumber.setValue(this.photo.getCode());
	}
	
}
