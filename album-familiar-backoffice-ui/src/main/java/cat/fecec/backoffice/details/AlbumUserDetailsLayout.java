/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.details;

import org.jamgo.layout.details.UserDetailsLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cat.fecec.model.AlbumUser;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AlbumUserDetailsLayout extends UserDetailsLayout<AlbumUser>{

	private static final long serialVersionUID = 1L;

	@Override
	protected Class<AlbumUser> getTargetObjectClass() {
		return AlbumUser.class;
	}

}
