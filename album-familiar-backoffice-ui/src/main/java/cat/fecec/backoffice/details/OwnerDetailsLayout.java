/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.details;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.services.UserService;
import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.jamgo.vaadin.ui.BinaryResourceTransferField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.viritin.fields.EmailField;

import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import cat.fecec.model.AlbumUser;
import cat.fecec.model.Owner;
import cat.fecec.model.Photo;
import cat.fecec.services.PhotoService;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OwnerDetailsLayout extends CrudDetailsLayout<Owner> {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(OwnerDetailsLayout.class);

	@Autowired
	private JamgoComponentFactory componentFactory;
	@Autowired
	private PhotoService photoService;
	@Autowired
	private UserService userService;

	private CrudDetailsPanel mainPanel;
	private VerticalLayout photosLayout = new VerticalLayout();
	private Label ownerRegistryNumber;
	private BinaryResourceTransferField ownerPicture;
	private Button addPictureButton;
	
	@Override
	protected List<CrudDetailsPanel> createPanels() {
		List<CrudDetailsPanel> panels = super.createPanels();
		return panels;
	}
	
	@Override
	protected CrudDetailsPanel createMainPanel() {
		this.mainPanel = this.componentFactory.newCrudDetailsPanel();
		this.mainPanel.setName(this.messageSource.getMessage("form.basic.info"));
		
		this.ownerPicture = this.componentBuilderFactory.createBinaryResourceTransferFieldBuilder().setCaption("owner.photo").build();
		this.ownerPicture.setPreviewEnabled(true);
		this.ownerPicture.setDescriptionEnabled(false);
		this.ownerPicture.setDateEnabled(false);
		this.ownerPicture.setHeight(200, Unit.PIXELS);
		this.ownerPicture.setSucceededListener(listener -> {
			byte[] content = listener.getBinaryObject().getContents();
			this.targetObject.setPicture(content);

			InputStream is = null;
			try {
				BinaryResource binaryResource = new BinaryResource();
				binaryResource.setContents(content);
				binaryResource.setMimeType("image/jpeg");
				binaryResource.setFileLength(content.length);
				binaryResource.setFileName(PhotoService.PARCTIPANT_PICTURE_FILE_NAME);
				this.ownerPicture.setValue(binaryResource);
				this.ownerPicture.setHeight(200, Unit.PIXELS);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						OwnerDetailsLayout.logger.error(e.getMessage(), e);
					}
				}
			}
		});
		
		FormLayout ownerFormLayout = new FormLayout();
		ownerFormLayout.setWidth(100, Unit.PERCENTAGE);
		
		this.ownerRegistryNumber = new Label();
		this.ownerRegistryNumber.setCaption(this.messageSource.getMessage("photo.registryNumber"));
		ownerFormLayout.addComponent(this.ownerRegistryNumber);
		
		TextField nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("owner.name")
				.setWidth(50, Unit.PERCENTAGE).build();
		ownerFormLayout.addComponent(nameField);
		this.binder.bind(nameField, Owner::getName, Owner::setName);
		
		TextField surnameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("owner.surnames")
				.setWidth(50, Unit.PERCENTAGE).build();
		ownerFormLayout.addComponent(surnameField);
		this.binder.bind(surnameField, Owner::getSurnames, Owner::setSurnames);
		
		
		TextField birthPlaceField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("owner.birth.place")
				.setWidth(50, Unit.PERCENTAGE).build();
		ownerFormLayout.addComponent(birthPlaceField);
		this.binder.bind(birthPlaceField, Owner::getBirthPlace, Owner::setBirthPlace);
		
		DateField birthDateField = new DateField(this.messageSource.getMessage("owner.birth.date"));
		ownerFormLayout.addComponent(birthDateField);
		this.binder.forField(birthDateField)
					.withConverter(new LocalDateToDateConverter())
					.bind(Owner::getBirthDate, Owner::setBirthDate);

		DateField interviewDateField = new DateField(this.messageSource.getMessage("owner.interviewDate"));
		ownerFormLayout.addComponent(interviewDateField);
		this.binder.forField(interviewDateField)
					.withConverter(new LocalDateToDateConverter())
					.bind(Owner::getInterviewDate, Owner::setInterviewDate);
		
		TextField phoneField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("owner.phone")
				.setWidth(50, Unit.PERCENTAGE).build();
		ownerFormLayout.addComponent(phoneField);
		this.binder.bind(phoneField, Owner::getPhone, Owner::setPhone);
		
		EmailField emailField = this.componentBuilderFactory.createEmailFieldBuilder().setCaption("owner.email")
				.setWidth(50, Unit.PERCENTAGE).build();
		ownerFormLayout.addComponent(emailField);
		this.binder.bind(emailField, Owner::getEmail, Owner::setEmail);
		
		
		TextArea otherInfoField = this.componentBuilderFactory.createTextAreaBuilder().setCaption("owner.otherInfo")
				.setWidth(100, Unit.PERCENTAGE).build();
		otherInfoField.setMaxLength(5000);
		ownerFormLayout.addComponent(otherInfoField);
		this.binder.bind(otherInfoField, Owner::getOtherInformation, Owner::setOtherInformation);
		
		HorizontalLayout ownerLayout = new HorizontalLayout();
		ownerLayout.setWidth(100, Unit.PERCENTAGE);
		ownerLayout.addComponents(this.ownerPicture, ownerFormLayout);
		ownerLayout.setExpandRatio(ownerFormLayout, 1);
		this.mainPanel.addComponent(ownerLayout);
		this.mainPanel.addComponent(this.photosLayout);
		
		return this.mainPanel;
	}
	
	@Override
	public void updateFields(Object savedItem) {
		super.updateFields(savedItem);
		this.photosLayout.removeAllComponents();
		
		for(int i=1 ; i < this.tabSheet.getComponentCount();) {
			Tab tab = this.tabSheet.getTab(i);
			this.tabSheet.removeComponent(tab.getComponent());
		}
		
		Owner owner = (Owner)savedItem;
		
		if (owner.getId() == null) {
			this.ownerPicture.setValue(null);
		} else {
			byte[] ownerPictureContent = this.photoService.getOwnerPicture(owner);
			BinaryResource binaryResource = null;
			if (ownerPictureContent != null) {
				binaryResource = new BinaryResource();
				binaryResource.setMimeType("image/jpg");
				binaryResource.setFileName(PhotoService.PARCTIPANT_PICTURE_FILE_NAME);
				binaryResource.setFileLength(ownerPictureContent.length);
				binaryResource.setContents(ownerPictureContent);
			}
			this.ownerPicture.setValue(binaryResource);
		}
		// initialize for default photo tab
		if ((owner.getPhotos() == null) || owner.getPhotos().isEmpty()) {
			Photo photo = new Photo();
			photo.setOwner(owner);
			owner.getPhotos().add(photo);
		}
		
		if ((owner.getPhotos() != null) && !owner.getPhotos().isEmpty()) {
			owner.getPhotos().stream().forEach(photo -> this.addPictureLayout(photo));
		}
		this.addNewPictureButton();
		
		AlbumUser user = (AlbumUser)this.userService.getCurrentUser();
//		if (!user.equals(owner.getUser()) && !VaadinRequest.getCurrent().isUserInRole("role_super_admin")) {
//			this.getOkButton().setVisible(false);
//		} else {
//			this.getOkButton().setVisible(true);
//		}
	}
	
	private void addNewPictureButton() {
		this.addPictureButton = new Button(this.messageSource.getMessage("action.add.picture"));
		this.addPictureButton.addStyleName(ValoTheme.BUTTON_LINK);
		this.addPictureButton.addClickListener(listener -> {
			Photo newPhoto = new Photo();
			newPhoto.setOwner(this.targetObject);
			this.photoService.save(newPhoto);
			this.targetObject.getPhotos().add(newPhoto);
			this.addPictureLayout(newPhoto);
			this.photosLayout.addComponent(this.addPictureButton);
		});
		this.photosLayout.addComponent(this.addPictureButton);
	}

	@Override
	public void updateTargetObject() throws ValidationException {
		this.targetObject = this.photoService.save(this.targetObject);
		super.updateTargetObject();
	}

	@Override
	protected Class<Owner> getTargetObjectClass() {
		return Owner.class;
	}
	
	private void addPictureLayout(Photo photo) {
		PhotoDetailsLayout photoDetailsLayout = this.applicationContext.getBean(PhotoDetailsLayout.class);
		photoDetailsLayout.setPhoto(photo);
		CrudDetailsPanel crudDetailsPanel = this.componentFactory.newCrudDetailsPanel();
		crudDetailsPanel.setContent(photoDetailsLayout);
		Tab tab = this.tabSheet.addTab(crudDetailsPanel);
		tab.setCaption(this.messageSource.getMessage("owner.photo.title") + " " + this.tabSheet.getTabPosition(tab));
		
	}
	
//	@PostConstruct
//	public void createJpgs() {
//		for(int i = 1; i < 29; i++) {
//			File file = new File("/opt/album.content/" + i);
//			for(File each : file.listFiles()) {
//				if (each.getName().endsWith(".tiff")) {
//					try {
//						System.out.println(each.getAbsolutePath());
//						File jpgFile = new File(each.getAbsolutePath().replaceAll(".tiff", ".jpg"));
//						if (!jpgFile.exists()) {
//							byte[] jpgBytes = ImageService.convertoToJpg(new FileInputStream(each));
//							FileOutputStream fos = new FileOutputStream(jpgFile);
//							fos.write(jpgBytes);
//							fos.flush();
//							fos.close();
//						}
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			}
//			
//		}
//	}
}
