/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//package cat.fecec.backoffice.details.relationships;
//
//import java.util.List;
//
//import org.jamgo.ui.layout.relationships.ToManyLayout;
//import org.jamgo.ui.layout.relationships.ToManySearchLayout;
//import org.springframework.beans.factory.config.BeanDefinition;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Component;
//
//import cat.fecec.model.Activity;
//import cat.fecec.model.School;
//
//@Component
//@Scope(BeanDefinition.SCOPE_PROTOTYPE)
//public class SchoolActivitiesOneToManyLayout extends ToManyLayout<School, Activity>{
//
//
//	private static final long serialVersionUID = 1L;
//
//	@Override
//	public void updateTargetObject() {
//		this.targetObject.setActivities(this.relatedObjectsList);
//
//	}
//
//	@Override
//	protected void defineColumnsGrid() {
//		this.relatedObjectsGrid.addColumn(o -> o.getName() == null ? "" : o.getName()) .setCaption(this.messageSource.getMessage("general.name"));
//		this.relatedObjectsGrid.addColumn(o -> o.getDescription() == null ? "" : o.getDescription()) .setCaption(this.messageSource.getMessage("general.description"));
//		this.relatedObjectsGrid.addColumn(o -> o.getDate() == null ? "" : o.getDate()) .setCaption(this.messageSource.getMessage("activity.date"));
//		this.relatedObjectsGrid.addColumn(o -> o.getPlace() == null ? "" : o.getPlace()) .setCaption(this.messageSource.getMessage("activity.place"));
//		this.relatedObjectsGrid.addColumn(o -> o.getDuration() == null ? "" : o.getDuration()) .setCaption(this.messageSource.getMessage("activity.duration"));
//	}
//
//	@Override
//	protected List<Activity> getRelatedObjects() {
//		return this.targetObject.getActivities();
//	}
//
//	@Override
//	protected ToManySearchLayout<Activity, ?> getManyToManySearchLayout() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//}
