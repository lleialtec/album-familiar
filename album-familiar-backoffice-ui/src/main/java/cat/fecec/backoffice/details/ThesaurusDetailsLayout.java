/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.details;

import java.util.List;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.utils.JamgoComponentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.data.TreeData;
import com.vaadin.data.ValidationException;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;

import cat.fecec.model.Thesaurus;
import cat.fecec.model.ThesaurusNode;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ThesaurusDetailsLayout extends CrudDetailsLayout<Thesaurus> {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(ThesaurusDetailsLayout.class);

	@Autowired
	private JamgoComponentFactory componentFactory;

	private CrudDetailsPanel mainPanel;
	private Tree<ThesaurusNode> tree;
	private TreeData<ThesaurusNode> thesaurusData = new TreeData<>();
	private DataProvider<ThesaurusNode,?> thesaurusDataProvider = new TreeDataProvider<ThesaurusNode>(this.thesaurusData);
	
	@Override
	protected List<CrudDetailsPanel> createPanels() {
		List<CrudDetailsPanel> panels = super.createPanels();
		return panels;
	}
	
	@Override
	protected CrudDetailsPanel createMainPanel() {
		this.mainPanel = this.componentFactory.newCrudDetailsPanel();
		this.mainPanel.setName(this.messageSource.getMessage("form.basic.info"));
		
		FormLayout thesaurusFormLayout = new FormLayout();
		thesaurusFormLayout.setWidth(100, Unit.PERCENTAGE);
		
		TextField nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("thesaurus.name")
				.setWidth(100, Unit.PERCENTAGE).build();
		thesaurusFormLayout.addComponent(nameField);
		this.binder.bind(nameField, Thesaurus::getName, Thesaurus::setName);
		
		TextField descriptionField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("thesaurus.description")
				.setWidth(100, Unit.PERCENTAGE).build();
		thesaurusFormLayout.addComponent(descriptionField);
		this.binder.bind(descriptionField, Thesaurus::getDescription, Thesaurus::setDescription);
		
		this.mainPanel.addComponent(thesaurusFormLayout);
		
		this.tree = new Tree<>(this.messageSource.getMessage("thesaurus.tree"));
		this.tree.setDataProvider(this.thesaurusDataProvider);
		this.tree.setItemCaptionGenerator(node -> node.getName());
		this.tree.setWidth(25, Unit.PERCENTAGE);
		thesaurusFormLayout.addComponent(this.tree);
		
		return this.mainPanel;
	}
	
	@Override
	public void updateFields(Object savedItem) {
		super.updateFields(savedItem);

		this.thesaurusData.clear();
		this.thesaurusData.addItems(((Thesaurus)savedItem).getThesaurusNodes(), ThesaurusNode::getChildren);
		this.thesaurusDataProvider.refreshAll();
	}
	
	@Override
	public void updateTargetObject() throws ValidationException {
		super.updateTargetObject();
	}

	@Override
	protected Class<Thesaurus> getTargetObjectClass() {
		return Thesaurus.class;
	}
}
