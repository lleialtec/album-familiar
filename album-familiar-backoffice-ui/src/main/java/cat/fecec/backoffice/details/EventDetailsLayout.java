/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.details;

import java.util.List;

import org.jamgo.ui.layout.crud.CrudDetailsLayout;
import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextField;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EventDetailsLayout extends CrudDetailsLayout<cat.fecec.model.event.Event>{

	private static final long serialVersionUID = 1L;

	@Override
	protected List<CrudDetailsPanel> createPanels() {
		List<CrudDetailsPanel> panels = super.createPanels();
		return panels;
	}

	@Override
	protected CrudDetailsPanel createMainPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();
		panel.setName(this.messageSource.getMessage("form.basic.info"));

		TextField nameField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("general.name").setWidth(50, Unit.PERCENTAGE).build();
		panel.addComponent(nameField);
		this.binder.bind(nameField, cat.fecec.model.event.Event::getName, cat.fecec.model.event.Event::setName);

		return panel;
	}

	@Override
	protected Class<cat.fecec.model.event.Event> getTargetObjectClass() {
		return cat.fecec.model.event.Event.class;
	}

}