/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.backoffice.search;

import org.jamgo.ui.layout.crud.CrudDetailsPanel;
import org.jamgo.ui.layout.crud.CrudSearchLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.TextField;

import cat.fecec.model.Photo;
import cat.fecec.model.repository.search.PhotoSearch;
import cat.fecec.model.repository.search.PhotoSpecification;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PhotoSearchLayout extends CrudSearchLayout<Photo, PhotoSearch>{

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	public void initSearchLayout() {
	}
	
	@Override
//	@PostConstruct
	public void initialize() {
		super.initialize();
		
		this.searchSpecification = this.applicationContext.getBean(PhotoSpecification.class);
	}

	@Override
	protected CrudDetailsPanel createPanel() {
		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel();

		TextField searchTextField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("photo.search.text").setWidth(100, Unit.PERCENTAGE).build();
		this.binder.bind(searchTextField, PhotoSearch::getText, PhotoSearch::setText);
		panel.addComponent(searchTextField);

		return panel;
	}

	@Override
	protected Class<PhotoSearch> getTargetObjectClass() {
		return PhotoSearch.class;
	}

}
