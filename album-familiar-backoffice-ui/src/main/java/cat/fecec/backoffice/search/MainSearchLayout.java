/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//package cat.fecec.backoffice.search;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//
//import org.jamgo.model.search.SearchSpecification;
//import org.jamgo.services.DatasourceServices;
//import org.jamgo.ui.layout.crud.CrudDetailsPanel;
//import org.jamgo.ui.layout.crud.CrudManager;
//import org.jamgo.ui.layout.crud.CrudSearchLayout;
//import org.jamgo.ui.layout.utils.JamgoComponentFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.config.BeanDefinition;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Component;
//
//import com.vaadin.ui.Button;
//import com.vaadin.ui.Grid;
//import com.vaadin.ui.TextField;
//
//import cat.fecec.model.Photo;
//import cat.fecec.model.repository.search.PhotoSearch;
//import cat.fecec.model.repository.search.PhotoSpecification;
//import cat.fecec.services.PhotoService;
//
//@Component
//@Scope(BeanDefinition.SCOPE_PROTOTYPE)
//public class MainSearchLayout extends org.jamgo.ui.layout.crud.CrudSearchLayout<Photo, PhotoSearch> {
//
//	private static final long serialVersionUID = 1L;
//
//	@Autowired
//	private JamgoComponentFactory componentFactory;
//	@Autowired
//	private ApplicationContext applicationContext;
//	@Autowired
//	protected DatasourceServices datasourceServices;
//	@Autowired
//	protected CrudManager crudManager;
//
//	@Autowired
//	private PhotoService photoService;
//	
//	private TextField fullTextField;
//
//	private Grid<Photo> resultGrid;
//
//	public static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");
//
//	private String[] columnHeaders;
//	private String[] visibleColumns = new String[] { "photoTitle", "author" };
//
//	@Override
//	@PostConstruct
//	public void initialize() {
//		super.initialize();
//		
//		this.resultGrid = new Grid<>();
//		this.resultGrid.setHeaderVisible(true);
//		this.resultGrid.addColumn(Photo::getPhotoTitle).setId("photoTitle").setCaption(this.messageSource.getMessage("photo.title"));
//		this.resultGrid.addColumn(Photo::getAuthor).setId("author").setCaption(this.messageSource.getMessage("photo.author"));
//		this.resultGrid.setWidth(100, Unit.PERCENTAGE);
//		this.addComponent(this.resultGrid);
//		
//		this.searchSpecification = this.applicationContext.getBean(PhotoSpecification.class);
//		
//		this.setApplyHandler(obj -> this.applySearch(obj));
//		
//		this.actionButtons.get(0).setCaption(this.messageSource.getMessage("action.search"));
//		this.actionButtons.get(1).setCaption(this.messageSource.getMessage("action.clean"));
//		this.actionButtons.get(1).addClickListener(e -> {
//			this.fullTextField.clear();
//			this.resultGrid.setItems(new ArrayList<>());
//		});
//		
//		// Column headers for export 
//		this.columnHeaders = new String[] { this.messageSource.getMessage("socialEconomyEntity.name"),
//			this.messageSource.getMessage("socialEconomyEntity.nif"),
//			this.messageSource.getMessage("socialEconomyEntity.sector"),
//			this.messageSource.getMessage("socialEconomyEntity.address"),
//			this.messageSource.getMessage("socialEconomyEntity.phone"),
//			this.messageSource.getMessage("socialEconomyEntity.web"),
//			this.messageSource.getMessage("socialEconomyEntity.postalCode"),
//			this.messageSource.getMessage("socialEconomyEntity.email"),
//			this.messageSource.getMessage("socialEconomyEntity.memberOf") };
//	}
//
//	protected void applySearch(CrudSearchLayout<Photo, PhotoSearch> searchLayout) {
//		SearchSpecification<Photo, PhotoSearch> searchSpecification = searchLayout.getSearchSpecification();
//		this.searchSpecification.setSearchObject(searchSpecification.getSearchObject());
//		this.resultGrid.setItems(this.photoService.search(searchSpecification.getSearchObject()));
//	}
//	
//	@Override
//	protected CrudDetailsPanel createPanel() {
//		CrudDetailsPanel panel = this.componentFactory.newCrudDetailsPanel(org.jamgo.ui.layout.crud.CrudDetailsPanel.Format.Vertical);
//		panel.setName(this.messageSource.getMessage("form.basic.info"));
//		this.fullTextField = this.componentBuilderFactory.createTextFieldBuilder().setCaption("socialEconomyEntity.search.fullText").setWidth(100, Unit.PERCENTAGE).build();
//		this.fullTextField.setWidth(100, Unit.PERCENTAGE);
//		panel.addComponent(this.fullTextField);
//		this.binder.bind(this.fullTextField, PhotoSearch::getText, PhotoSearch::setText);
//		return panel;
//	}
//
//	@Override
//	protected Class<PhotoSearch> getTargetObjectClass() {
//		return PhotoSearch.class;
//	}
//}
