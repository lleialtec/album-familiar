/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.jamgo.model.entity.BinaryResource;
import org.jamgo.model.entity.Model;
import org.jamgo.model.entity.Town;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "photo")
public class Photo extends Model{

	private static final long serialVersionUID = 1L;
	
	public enum Side {
		REV, ANV
	}
	
	public enum Color {
		BlackWhite, Color
	}

	@Id
	@TableGenerator(name = "PHOTO_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "PHOTO_SEQ", initialValue = 1000, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PHOTO_ID_GEN")
	private Long id;
	
	@Column(name = "registry_number")
	private String registryNumber;

	@Column(name = "author")
	private String author;
	
	@Column(name = "photo_title")
	private String photoTitle;
	
	@JoinColumn(name = "photo_id")
	private BinaryResource photo;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "place")
	private String place;
	
	@Column(name = "annotations")
	private String annotations;
	
	@Column(name = "descriptors")
	private String descriptors;
	
	@JoinColumn(name = "id_town")
	private Town town;
	
	@Column(name = "description_author")
	private String descriptionAuthor;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "description_date")
	private Date descriptionDate;
	
	@Column(name = "given_by")
	private String givedBy;
	
	@Column(name = "source")
	private String source;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "side")
	private Side side;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_owner")
	private Owner owner;
	
	@Column(name = "size")
	private String size;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "color")
	private Color color;
	
	@Column(name = "technical_description")
	private String technicalDescription;
	
//	@Column(name = "location")
//	private Point location;
	
	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longitude;
	
	@Column(name = "other_info", length = 5000)
	private String otherInformation;

	@JsonIgnore
	@Transient
	private Map<Side, byte[]> tiffContents = new HashMap<Photo.Side, byte[]>();
	
	@JsonIgnore
	@Transient
	private Map<Side, byte[]> jpgContents = new HashMap<Photo.Side, byte[]>();
	
	@JsonIgnore
	@Transient
	private byte[] audioContent;
	
	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Town getTown() {
		return this.town;
	}

	public void setTown(Town town) {
		this.town = town;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPhotoTitle() {
		return this.photoTitle;
	}

	public void setPhotoTitle(String photoTitle) {
		this.photoTitle = photoTitle;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getAnnotations() {
		return this.annotations;
	}

	public void setAnnotations(String annotations) {
		this.annotations = annotations;
	}

	public String getDescriptionAuthor() {
		return this.descriptionAuthor;
	}

	public void setDescriptionAuthor(String descriptionAuthor) {
		this.descriptionAuthor = descriptionAuthor;
	}

	public Date getDescriptionDate() {
		return this.descriptionDate;
	}

	public void setDescriptionDate(Date descriptionDate) {
		this.descriptionDate = descriptionDate;
	}

	public String getGivedBy() {
		return this.givedBy;
	}

	public void setGivedBy(String givedBy) {
		this.givedBy = givedBy;
	}

	public BinaryResource getPhoto() {
		return this.photo;
	}

	public void setPhoto(BinaryResource photo) {
		this.photo = photo;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Owner getOwner() {
		return this.owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public String getDescriptors() {
		return this.descriptors;
	}

	public void setDescriptors(String descriptors) {
		this.descriptors = descriptors;
	}

	public String getRegistryNumber() {
		return this.registryNumber;
	}

	public void setRegistryNumber(String registryNumber) {
		this.registryNumber = registryNumber;
	}
	
	public static String getRegistry(String registryNumber, String side) {
		String contentRegistryBase = "AFS-"; 
		for (int i = 0; i < (5 - registryNumber.length()); i++) {
			contentRegistryBase = contentRegistryBase.concat("0");
		}
		return contentRegistryBase.concat(registryNumber).concat(side);
	}

	public Side getSide() {
		return this.side;
	}

	public void setSide(Side side) {
		this.side = side;
	}

	public byte[] getAudioContent() {
		return this.audioContent;
	}

	public void setAudioContent(byte[] audioContent) {
		this.audioContent = audioContent;
	}

	public Map<Side, byte[]> getTiffContents() {
		return this.tiffContents;
	}

	public void setTiffContents(Map<Side, byte[]> tiffContents) {
		this.tiffContents = tiffContents;
	}

	public Map<Side, byte[]> getJpgContents() {
		return this.jpgContents;
	}

	public void setJpgContents(Map<Side, byte[]> jpgContents) {
		this.jpgContents = jpgContents;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getTechnicalDescription() {
		return this.technicalDescription;
	}

	public void setTechnicalDescription(String technicalDescription) {
		this.technicalDescription = technicalDescription;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getOtherInformation() {
		return this.otherInformation;
	}

	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}
	
	public String getCode() {
		return this.getCode(null);
	}
	
	public String getCode(Side side) {
		StringBuffer code = new StringBuffer("AFS-");
		if ((this.id != null) && (this.id != 0)) {
			for (int i = 0; i < (5 - this.id.toString().length()); i++) {
				code.append("0");
			}
			code.append(this.id);
		} else {
			code.append("XXXXX");
		}
		if (side != null) {
			code.append(side.name());
		}
		code.append("-");
		code.append(Optional.ofNullable(this.owner.getInitials()).orElse("XXX"));
		return code.toString().toUpperCase();
	}
}
