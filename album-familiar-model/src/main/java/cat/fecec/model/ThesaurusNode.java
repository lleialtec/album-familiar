/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.jamgo.model.entity.Model;

@Entity
@Table(name = "thesaurus_node")
public class ThesaurusNode extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "THESAURUS_CONCEPT_ID_GEN", table = "sequence_table", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "THESAURUS_CONCEPT_SEQ", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "THESAURUS_CONCEPT_ID_GEN")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "id_thesaurus")
	private Thesaurus thesaurus;
	
	@OneToMany(mappedBy = "parent")
	private List<ThesaurusNode> children;
	
	@ManyToOne
	@JoinColumn(name = "id_parent")
	private ThesaurusNode parent;
	
	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public ThesaurusNode getParent() {
		return this.parent;
	}

	public void setParent(ThesaurusNode parent) {
		this.parent = parent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ThesaurusNode> getChildren() {
		return this.children;
	}

	public void setChildren(List<ThesaurusNode> children) {
		this.children = children;
	}

}
