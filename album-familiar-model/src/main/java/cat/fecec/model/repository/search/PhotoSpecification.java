/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.model.repository.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.queryparser.classic.ParseException;
import org.jamgo.model.search.SearchSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cat.fecec.model.Photo;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PhotoSpecification extends SearchSpecification<Photo, PhotoSearch> {

	private static final Logger logger = LoggerFactory.getLogger(PhotoSpecification.class);
	
	@Autowired
	private FullTextService fullTextService;
	
	public PhotoSpecification() {
		this.searchObject = new PhotoSearch();
	}

	@Override
	public Predicate toPredicate(Root<Photo> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (StringUtils.isNotBlank(this.searchObject.getText())) {
			Set<Long> entityIds = new HashSet<>();
			try {
				entityIds = this.fullTextService.findEntities(this.searchObject.getText());
			} catch (IOException | ParseException e) {
				logger.error(e.getMessage(), e);
			}
			if (!entityIds.isEmpty()) {
				predicates.add(root.get("id").in(entityIds));
			} else {
				// ...	FIXME
				predicates.add(cb.equal(root.get("id"), 0));
			}
		}

//		if (StringUtils.isNotBlank(this.searchObject.getAuthor())) {
//			predicates.add(cb.like(cb.lower(root.get("author")), "%" + this.searchObject.getAuthor().toLowerCase() + "%"));
//		}
//
//		if (StringUtils.isNotBlank(this.searchObject.getPhotoTitle())) {
//			predicates.add(cb.like(cb.lower(root.get("photoTitle")), "%" + this.searchObject.getPhotoTitle().toLowerCase() + "%"));
//		}

		return this.andTogether(root, predicates, cb);
	}

}
