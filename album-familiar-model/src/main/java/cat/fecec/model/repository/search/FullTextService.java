/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.model.repository.search;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;

import cat.fecec.model.Photo;
import cat.fecec.model.ThesaurusNode;
import cat.fecec.model.repository.PhotoRepository;
import cat.fecec.model.repository.ThesaurusNodeRepository;

@Service
public class FullTextService {

	@Autowired
	private PhotoRepository photoRepository;
	@Autowired
	private ThesaurusNodeRepository thesaurusNodeRepository;

	private Analyzer analyzer;
	private Directory directory;
	private IndexWriterConfig config;
	private IndexWriter writer;

	@PostConstruct
	private void init() throws IOException {
//		this.analyzer = new CatalanAnalyzer();
		this.analyzer = new SimpleAnalyzer();
		this.directory = new RAMDirectory();
		this.config = new IndexWriterConfig(this.analyzer);
		this.writer = new IndexWriter(this.directory, this.config);
		// TODO Improve performance: for each entity, select to database for sectors, economic activity,...
		for (Photo photo : this.photoRepository.findAll()) {
			this.indexEntity(photo);
		}
	}

	public void indexEntity(Photo photo) throws IOException {
		Document document = new Document();
		document.add(new StoredField("id", photo.getId()));
		if (photo.getAuthor() != null) {
			document.add(new TextField("author", photo.getAuthor(), Store.YES));
		}

		if (photo.getDescriptionAuthor() != null) {
			document.add(new TextField("descriptionAuthor", photo.getDescriptionAuthor(), Store.YES));
		}

		if (photo.getDescriptors() != null) {
			document.add(new TextField("descriptors", photo.getDescriptors(), Store.YES));
		}

		if (photo.getGivedBy() != null) {
			document.add(new TextField("givenBy", photo.getGivedBy(), Store.YES));
		}

		if (photo.getOtherInformation() != null) {
			document.add(new TextField("otherInformation", photo.getOtherInformation(), Store.YES));
		}

		if (photo.getPhotoTitle() != null) {
			document.add(new TextField("photoTitle", photo.getPhotoTitle(), Store.YES));
		}

		if (photo.getPlace() != null) {
			document.add(new TextField("place", photo.getPlace(), Store.YES));
		}

		if (photo.getSource() != null) {
			document.add(new TextField("source", photo.getSource(), Store.YES));
		}

		if (photo.getTechnicalDescription() != null) {
			document.add(new TextField("technicalDescription", photo.getTechnicalDescription(), Store.YES));
		}

		this.writer.addDocument(document);
		this.writer.commit();
	}

	public Set<Long> findEntities(String searchText) throws IOException, ParseException {
		String[] searchTextParts = searchText.split(" ");
		for(String text : searchTextParts) {
			ThesaurusNode thesaurusNode = this.thesaurusNodeRepository.findByName(text);
			if (thesaurusNode != null) {
				searchText = searchText.concat(" ").concat(this.feedWithThesaurus(text, thesaurusNode));
			}
		}
		
		IndexReader reader = DirectoryReader.open(this.directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		String[] fields = { "author", "descriptionAuthor", "descriptors", "givenBy", "otherInformation", "photoTitle", "place" ,"source","technicalDescription"};
		BooleanClause.Occur[] flags = {
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD,
			BooleanClause.Occur.SHOULD
		};
		Query query = MultiFieldQueryParser.parse(searchText, fields, flags, this.analyzer);
		TopDocs topDocs = searcher.search(query, 900);
		Set<Long> entityIds = new LinkedHashSet<>();
		for (ScoreDoc eachScoreDoc : topDocs.scoreDocs) {
			Document eachDoc = searcher.doc(eachScoreDoc.doc, Sets.newHashSet("id"));
			entityIds.add(Long.valueOf(eachDoc.get("id")));
		}
		return entityIds;
	}
	
	private String feedWithThesaurus(String text, ThesaurusNode node) {
		text = text.concat(node.getName()).concat(" ");
		for(ThesaurusNode childNode : node.getChildren()) {
			text = this.feedWithThesaurus(text, childNode);
		}
		return text;
	}

}