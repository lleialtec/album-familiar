/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.model.repository.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jamgo.model.search.SearchSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cat.fecec.model.Owner;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OwnerSpecification extends SearchSpecification<Owner, Owner> {

	private static final Logger logger = LoggerFactory.getLogger(OwnerSpecification.class);
	
	public OwnerSpecification() {
		this.searchObject = new Owner();
	}

	@Override
	public Predicate toPredicate(Root<Owner> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (Optional.ofNullable(this.searchObject).isPresent()) {
			predicates.add(cb.equal(root.get("id"), this.searchObject.getId()));
		}

		return this.andTogether(root, predicates, cb);
	}

}
