/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.services;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ImageService {
	
	private static Logger logger = LoggerFactory.getLogger(ImageService.class);

//	public static byte[] convertoToJpg(InputStream is) {
//		System.out.println("Converting to jpg...");
//		ByteArrayOutputStream os = new ByteArrayOutputStream();
//		try {
//			BufferedImage img = ImageIO.read(is);
//			
////			for (String type : ImageIO.getWriterFormatNames()) {
////				System.out.println(type);
////				if (type.equalsIgnoreCase("jpg") || type.equalsIgnoreCase("jpeg")) {
//					// Avoid issue #6 on OpenJDK8/Debian 
////					continue;
////				}
//				ImageIO.write(img, "jpg", os);
////				ImageIO.read(f);
//				os.flush();
//				return os.toByteArray();
////			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	public static byte[] convertoToJpg(InputStream is, Consumer<Integer> progressConsumer) {
		System.out.println("-Converting to jpg...");
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			BufferedImage image = ImageIO.read(is);
//			BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
			System.out.println("Creating buffered image");
			BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
			System.out.println("Setting pixel by pixel...");
		    for (int x = 0; x < image.getWidth(); x++) {
		        for (int y = 0; y < image.getHeight(); y++) {
		        	progressConsumer.accept(Integer.valueOf((x/image.getWidth()) + (y / image.getHeight())));
		            newImage.setRGB(x, y, image.getRGB(x, y));
		        }
		    }
		    System.out.println("Pixels set");
			ImageIO.write(newImage, "jpeg", os);
			System.out.println("Writing to outputstream");
			os.flush();
			return os.toByteArray();
		} catch (IOException e) {
			ImageService.logger.error(e.getMessage(), e);
		} finally {
			try {
				os.close();
			} catch (IOException e) {
				ImageService.logger.error(e.getMessage());
			}
		}
		return null;
	}
	
//	 public static void TiffToJpg(String tiff, String output)
//		     throws IOException
//		  {
//		    File tiffFile = new File(tiff);
//		    SeekableStream s = new FileSeekableStream(tiffFile);
//		    TIFFDecodeParam param = null;
//		    ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, param);
//		    RenderedImage op = dec.decodeAsRenderedImage(0);
//		    FileOutputStream fos = new FileOutputStream(output);
//		    JPEGImageEncoder jpeg = JPEGCodec.createJPEGEncoder(fos);
//		    jpeg.encode(op.getData());
//		    fos.close();
//		  }
	
	public static InputStream addReferenceMark(InputStream is, String mark) {
		ByteArrayOutputStream os = null;
		try {
			BufferedImage image = ImageIO.read(is);
			Graphics g = image.getGraphics();
			g.drawString(mark, 2, 2);
			g.dispose();
			os = new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", os);
			return new ByteArrayInputStream(os.toByteArray());
		} catch (IOException e) {
			ImageService.logger.error(e.getMessage());
		} finally {
			try {
				if (os != null) {
					os.close();
				}
			} catch (IOException e) {
				ImageService.logger.error(e.getMessage());
			}
		}
		return null;
	}
	
	public static byte[] addReferenceMark(byte[] content, String mark) {
		ByteArrayOutputStream os = null;
		ByteArrayInputStream is = new ByteArrayInputStream(content);
		try {
			BufferedImage image = ImageIO.read(is);
			Graphics g = image.getGraphics();
			g.setFont(g.getFont().deriveFont(40f));
			g.setColor(Color.WHITE);
			g.drawString(mark, 20, image.getHeight() - 20);
			g.dispose();
			os = new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", os);
			byte[] contentWithMark = os.toByteArray();
			return contentWithMark;
		} catch (IOException e) {
			ImageService.logger.error(e.getMessage());
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				ImageService.logger.error(e.getMessage());
			}
		}
		return null;
	}
	
}
