/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.watson.developer_cloud.http.HttpMediaType;
import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechResults;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.Transcript;

import cat.fecec.model.Owner;
import cat.fecec.model.Photo;
import cat.fecec.model.Photo.Side;
import cat.fecec.model.repository.OwnerRepository;
import cat.fecec.model.repository.PhotoRepository;
import cat.fecec.model.repository.search.FullTextService;
import cat.fecec.model.repository.search.PhotoSearch;
import cat.fecec.model.repository.search.PhotoSpecification;

@Service
public class PhotoService {
	
	private static final Logger logger = LoggerFactory.getLogger(PhotoService.class);
	
	private static final String MEDIA_PATH = "/opt/album.content/";
	public static final String PARCTIPANT_PICTURE_FILE_NAME = "participant.jpg";
	
	public static final String TIFF_EXTENSION = "tiff";
	public static final String JPG_EXTENSION = "jpg";
	public static final String WAV_EXTENSION = "wav";
	
	@Autowired
	private OwnerRepository ownerRepository;
	@Autowired
	private PhotoRepository photoRepository;
	@Autowired
	private PhotoSpecification photoSearchSpecification;
	@Autowired
	private FullTextService fullTextService;

	public List<Photo> search(PhotoSearch photoSearch) {
		this.photoSearchSpecification.setSearchObject(photoSearch);
		return this.photoRepository.findAll(this.photoSearchSpecification);
	}
	
	public Photo getPhoto(Long id) {
		return this.photoRepository.findOne(id);
	}
	
	public String transcryptAudio(byte[] audio) {
		FileOutputStream fos = null;	
		File tempFile = null;
		try {
			byte[] byteArray = Base64.getDecoder().decode(audio);
			tempFile = File.createTempFile("speech-", ".wav", null);
			fos = new FileOutputStream(tempFile);
			fos.write(byteArray);
		} catch (IOException e) {
			PhotoService.logger.error(e.getMessage(), e);
			return "";
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		SpeechToText service = new SpeechToText();
		service.setUsernameAndPassword( "<username>", "<password>" );
		        
		RecognizeOptions options = new RecognizeOptions.Builder()
		                .contentType(HttpMediaType.AUDIO_WAV) //select your format
		                .build();
		        
		SpeechResults result = service.recognize(tempFile, options).execute();
			
		String text = null;
		if( !result.getResults().isEmpty() ) {
		    for(Transcript transcript: result.getResults()){
		        if(transcript.isFinal()){
		            text = transcript.getAlternatives().get(0).getTranscript();
		            break;
		        }
		    }
		}
		return text;
	}
	
	public static List<byte[]> getPhoto(String ownerId, String photoId) throws FileNotFoundException {
		List<byte[]> photoStreams = new ArrayList<>();
		photoStreams.add(PhotoService.getPhoto(ownerId, photoId, Side.ANV));
		photoStreams.add(PhotoService.getPhoto(ownerId, photoId, Side.REV));
		return photoStreams;
	}
	
	public static byte[] getPhoto(String ownerId, String photoId, Photo.Side side) throws FileNotFoundException {
		File dir = new File(PhotoService.MEDIA_PATH + ownerId);
		if (dir.exists()) {
			for(File file : dir.listFiles()) {
				String fileName = file.getName().replace(dir.getPath(), "");
				String contentRegistryBase = "AFS-"; 
				for (int i = 0; i < (5 - photoId.length()); i++) {
					contentRegistryBase = contentRegistryBase.concat("0");
				}
				contentRegistryBase = contentRegistryBase.concat(photoId);
				if (side != null) {
					contentRegistryBase = contentRegistryBase.concat(side.name());
				}
				if (fileName.startsWith(contentRegistryBase) && fileName.endsWith(PhotoService.JPG_EXTENSION)) {
					InputStream is = null;
					try {
						is = new FileInputStream(file);
						byte[] bytes = IOUtils.toByteArray(is);
						return bytes;
//						photoStreams.add(bytes);
					} catch (IOException e) {
						PhotoService.logger.error(e.getMessage(), e);
					} finally {
						if (is != null) {
							try {
								is.close();
							} catch (IOException e) {
								PhotoService.logger.error(e.getMessage(), e);
							}
						}
					}
				}
			}
		}
//		return photoStreams;
		return null;
		
	}
	
	public static InputStream getAudio(String ownerId, String photoId) throws FileNotFoundException {
		InputStream photoStream = null;
		File dir = new File(PhotoService.MEDIA_PATH + ownerId);
		if (dir.exists()) {
			for(File file : dir.listFiles()) {
				String fileName = file.getName().replace(dir.getPath(), "");
				String contentRegistryBase = "AFS-"; 
				for (int i = 0; i < (5 - photoId.length()); i++) {
					contentRegistryBase = contentRegistryBase.concat("0");
				}
				contentRegistryBase = contentRegistryBase.concat(photoId);
				if (fileName.startsWith(contentRegistryBase) && fileName.endsWith(PhotoService.WAV_EXTENSION)) {
					photoStream = new FileInputStream(file);
				}
			}
		}
		return photoStream;
	}
	
	public static File getRegistryFile(String ownerId, String photoId, String side, String extension) {
		return new File(PhotoService.MEDIA_PATH + ownerId + "/" + Photo.getRegistry(photoId, side) + "." + extension);
	}
	
	@Transactional
	public Owner save(final Owner owner) {
		// get photos references before they are updated, the instance is changed
		// and transient content is lost
		byte[] ownerPhoto = owner.getPicture();
		List<Photo> photos = owner.getPhotos();
		Owner updatedOwner = this.ownerRepository.save(owner);
		updatedOwner.getPhotos().stream().forEach(photo -> {
			try {
				this.fullTextService.indexEntity(photo);
			} catch (IOException e) {
				PhotoService.logger.error(e.getMessage(), e);
			}
		});
		File file = new File(PhotoService.MEDIA_PATH + owner.getId());
		if (!file.exists()) {
			file.mkdir();
		}
		ObjectMapper objectMapper = new ObjectMapper(); 
		try {
			objectMapper.writeValue(new File(file.getPath() + "/data.json"), updatedOwner);
		} catch (IOException e) {
			PhotoService.logger.error(e.getMessage(), e);
		}
		if (ownerPhoto != null) {
			OutputStream os = null;
			try {
				os = new FileOutputStream(new File(PhotoService.MEDIA_PATH + owner.getId() + "/" + PhotoService.PARCTIPANT_PICTURE_FILE_NAME));
				StreamUtils.copy(ownerPhoto, os);
			} catch (IOException e) {
				PhotoService.logger.error(e.getMessage(), e);
			} finally {
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
						PhotoService.logger.error(e.getMessage(), e);
					}
				}
			}
		}
		photos.stream().forEach(photo -> {
			photo.getTiffContents().keySet().stream().forEach(side -> {
				OutputStream os = null;
				try {
					os = new FileOutputStream(PhotoService.getRegistryFile(String.valueOf(owner.getId()), String.valueOf(photo.getId()), side.name(), PhotoService.TIFF_EXTENSION));
					StreamUtils.copy(photo.getTiffContents().get(side), os);
				} catch (IOException e) {
					PhotoService.logger.error(e.getMessage(), e);
				} finally {
					if (os != null) {
						try {
							os.close();
						} catch (IOException e) {
							PhotoService.logger.error(e.getMessage(), e);
						}
					}
				}
			});
			photo.getJpgContents().keySet().stream().forEach(side -> {
				OutputStream os = null;
				try {
					os = new FileOutputStream(PhotoService.getRegistryFile(String.valueOf(owner.getId()), String.valueOf(photo.getId()), side.name(), PhotoService.JPG_EXTENSION));
					StreamUtils.copy(photo.getJpgContents().get(side), os);
				} catch (IOException e) {
					PhotoService.logger.error(e.getMessage(), e);
				} finally {
					if (os != null) {
						try {
							os.close();
						} catch (IOException e) {
							PhotoService.logger.error(e.getMessage(), e);
						}
					}
				}
			});
			
			if (photo.getAudioContent() != null) {
				byte[] audioContent = photo.getAudioContent();
				OutputStream os = null;
				try {
					os = new FileOutputStream(PhotoService.getRegistryFile(String.valueOf(owner.getId()), String.valueOf(photo.getId()), "", PhotoService.WAV_EXTENSION));
					StreamUtils.copy(audioContent, os);
				} catch (IOException e) {
					PhotoService.logger.error(e.getMessage(), e);
				} finally {
					if (os != null) {
						try {
							os.close();
						} catch (IOException e) {
							PhotoService.logger.error(e.getMessage(), e);
						}
					}
				}
			}
		});
		return updatedOwner;
	}
	
	public Photo save(Photo photo) {
		return this.photoRepository.save(photo);
	}

	public byte[] getOwnerPicture(Owner owner) {
		InputStream is = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			File file = new File(PhotoService.MEDIA_PATH + String.valueOf(owner.getId()) + "/" + PhotoService.PARCTIPANT_PICTURE_FILE_NAME);
			if (file.exists()) {
				is = new FileInputStream(file);
				StreamUtils.copy(is, bos);
				return bos.toByteArray();
			}
		} catch (IOException e) {
			PhotoService.logger.error(e.getMessage(), e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
				if (bos != null) {
					bos.close();
				}
			} catch (IOException e) {
				PhotoService.logger.error(e.getMessage(), e);
			}
		}
		return null;
	}

	public List<Photo> getPhotos() {
		return this.photoRepository.findAll();
	}
	
	public void deletePhoto(Photo photo) {
		photo.getOwner().getPhotos().remove(photo);
		this.photoRepository.delete(photo);
		String basePath =  PhotoService.MEDIA_PATH + photo.getOwner().getId() + "/" + photo.getCode();
		String basePathAnv =  PhotoService.MEDIA_PATH + photo.getOwner().getId() + "/" + photo.getCode(Side.ANV);
		String basePathRev =  PhotoService.MEDIA_PATH + photo.getOwner().getId() + "/" + photo.getCode(Side.REV);
		Arrays.asList(PhotoService.TIFF_EXTENSION, PhotoService.JPG_EXTENSION).forEach(extension -> {
			File fileAnv = new File(basePathAnv + "." + extension);
			if (fileAnv.exists()) {
				fileAnv.delete();
			}
			File fileRev = new File(basePathRev + "." + extension);
			if (fileRev.exists()) {
				fileRev.delete();
			}
		});
		File fileWav = new File(basePath + "." + PhotoService.WAV_EXTENSION);
		if (fileWav.exists()) {
			fileWav.delete();
		}
		
	}

}
