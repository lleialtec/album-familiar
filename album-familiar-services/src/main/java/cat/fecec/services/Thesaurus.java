/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.services;

import java.io.IOException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.springframework.stereotype.Component;

@Component
public class Thesaurus {

	private JsonObject thesaurus;
	
	public Thesaurus() {
		InputStream is = null;
		try {
			is = Thread.currentThread().getContextClassLoader().getResourceAsStream("thesaurus.json");
			this.thesaurus = Json.createReader(is).readObject();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String fillThesaurus(String searchText) {
		StringBuffer text = new StringBuffer();
		String[] searchTextParts = searchText.split(" ");
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		for(String searchTextPart : searchTextParts) {
			this.getValue(searchTextPart, this.thesaurus, jsonBuilder);
		}
		
		return searchText.concat(" ").concat(jsonBuilder.build().toString());
	}
	
	public void getValue(String key, JsonObject thes, JsonObjectBuilder jsonBuilder) {
		thes.keySet().stream().forEach(eachKey -> {
			JsonValue jsonValue = thes.get(eachKey);
			if (eachKey.equals(key)) {
				jsonBuilder.add(eachKey, jsonValue);
				return;
			}
			if (jsonValue instanceof JsonObject) {
				this.getValue(key, (JsonObject)jsonValue, jsonBuilder);
			} else if (jsonValue instanceof JsonArray) {
				((JsonArray)jsonValue).stream().forEach(eachValue -> {
					this.getValue(key, (JsonObject)eachValue, jsonBuilder);
				});
				
			}
			
		});
	}
	
	public static void main(String[] args) {
		System.out.println("asdf " + new Thesaurus().fillThesaurus("hola Técnica").toString());
	}
}
