/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.services;

import java.io.IOException;

import org.jamgo.model.entity.User;
import org.jamgo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDescriptor;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.makernotes.CanonMakernoteDescriptor;
import com.drew.metadata.exif.makernotes.CanonMakernoteDirectory;

import cat.fecec.model.AlbumUser;
import cat.fecec.model.repository.AlbumUserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private AlbumUserRepository userRepository;

	@Override
	public User getCurrentUser() {
		AlbumUser currentUser = null;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			currentUser = this.userRepository.findByUsername(authentication.getName());
		}
		return currentUser;
	}

	public static void main(String[] args) throws ImageProcessingException, IOException {
		Metadata metadata = ImageMetadataReader.readMetadata(Thread.currentThread().getContextClassLoader().getResourceAsStream("demoCR2Image.CR2"));
//		ExifDirectoryBase.TAG_ACCELERATION;
//		
		// obtain a specific directory
		ExifSubIFDDirectory exifSubIFDDirectory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
		// create a descriptor
		ExifSubIFDDescriptor exifSubIFDDescriptor = new ExifSubIFDDescriptor(exifSubIFDDirectory);
		// get tag description
		String program = exifSubIFDDescriptor.getExposureProgramDescription();
		System.out.println("Exposure program " + program);
		
		CanonMakernoteDirectory canonMakernoteDirectory = metadata.getFirstDirectoryOfType(CanonMakernoteDirectory.class);
		CanonMakernoteDescriptor canonMakernoteDescriptor = new CanonMakernoteDescriptor(canonMakernoteDirectory);
		String imageSize = canonMakernoteDescriptor.getImageSizeDescription();
		System.out.println("Image size " + imageSize);
		
//		https://github.com/gasrios/raw

	}
}

