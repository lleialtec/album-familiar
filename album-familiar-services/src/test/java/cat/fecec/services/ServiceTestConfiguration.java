/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cat.fecec.services;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.logging.SessionLog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;

@Configuration
@ComponentScan(basePackageClasses = { cat.fecec.services.PackageMarker.class, cat.fecec.model.PackageMarker.class })
public class ServiceTestConfiguration {

	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl("jdbc:h2:mem:testDb;MODE=MySql");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return dataSource;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		EclipseLinkJpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(true);
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(
			org.jamgo.model.PackageMarker.class.getPackage().getName(),
			cat.fecec.model.PackageMarker.class.getPackage().getName());
		factory.setDataSource(this.dataSource());
		factory.setJpaPropertyMap(this.getJpaPropertiesMap());
		return factory;
	}

	@Bean
	public JpaTransactionManager transactionManager() {
		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(this.entityManagerFactory().getObject());
		return txManager;
	}
	
	protected Map<String, Object> getJpaPropertiesMap() {
		final Map<String, Object> properties = new HashMap<>();
		properties.put(PersistenceUnitProperties.DEPLOY_ON_STARTUP, "true");
//		properties.put(PersistenceUnitProperties.DDL_GENERATION, "create-tables");
//		properties.put(PersistenceUnitProperties.DDL_SQL_SCRIPT_GENERATION, "true");
//		properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE, "sql-script");
		properties.put(PersistenceUnitProperties.SCHEMA_GENERATION_DATABASE_ACTION, PersistenceUnitProperties.SCHEMA_GENERATION_CREATE_ACTION);
		properties.put(PersistenceUnitProperties.WEAVING, "static");
		properties.put(PersistenceUnitProperties.LOGGING_LEVEL, SessionLog.INFO_LABEL);
		properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true");
//		properties.put(PersistenceUnitProperties.SCHEMA_GENERATION_SQL_LOAD_SCRIPT_SOURCE, "import-bs.sql");
		return properties;
	}
}
