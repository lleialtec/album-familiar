/*
 * Album Familiar Backoffice - Web application for managing family photographies and related meta data from Sants neighborhood.
 * Copyright (C) 2020  Lleialtat Santsenca
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//package cat.fecec.services;
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.URISyntaxException;
//import java.net.URL;
//import java.util.Locale;
//
//import javax.imageio.ImageIO;
//import javax.imageio.ImageReader;
//import javax.imageio.metadata.IIOMetadata;
//import javax.imageio.spi.ImageReaderSpi;
//import javax.imageio.stream.FileImageInputStream;
//
//import org.junit.runner.RunWith;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@Configuration
//@ContextConfiguration(classes = ServiceTestConfiguration.class )
//public class ImagesTest {
//
//
//	public static void main(String[] args) {
//		InputStream is = null;
//		try {
//			is = Thread.currentThread().getContextClassLoader().getResourceAsStream("AFS-00002ANV-RBG.tiff");
//			BufferedImage image = ImageIO.read(is);
//			ImageIO.write(image, "png", new File("/home/aritz/Downloads/test.png"));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			try {
//				if (is != null) {
//					is.close();
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//	
////	public static void main2(String[] args) {
////		InputStream is = null;
////		FileImageInputStream fiis = null;
////		try {
////			URL url = Thread.currentThread().getContextClassLoader().getResource("AFS-00002ANV-RBG.tiff");
////			fiis = new FileImageInputStream(new File(url.toURI()));
////			TIFFImageReader imageReader = new TIFFImageReader(new TIFFImageReaderSpi());
////			imageReader.setInput(fiis);
////			IIOMetadata metadata = imageReader.getStreamMetadata();
////			metadata.getController();
////		} catch (IOException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} catch (URISyntaxException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} finally {
////			try {
////				if (is != null) {
////					is.close();
////				}
////			} catch (IOException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
////		}
////	}
//}
