insert into user (id,username,password,enabled,version) values (1,'user1', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K',1,0);
insert into user (id,username,password,enabled,version) values (2,'user2', '$2a$11$hxzJhci3PfZMQnWls/LTdueqo/QWD2t9QBq7pLvLbFUwDRyaVF69K',1,0);

insert into role(id,rolename,version) values (1,'role_super_admin',0);
insert into role(id,rolename,version) values (2,'role_admin',0);
insert into role(id,rolename,version) values (3,'role_user',0);

insert into user_role(id_user,id_role) values (1, 1);
insert into user_role(id_user,id_role) values (2, 2);

insert into language(id,name,language_code, country_code, version) values (1,'Català','ca','ES',0);
insert into language(id,name,language_code, country_code, version) values (2,'Castellano','es','ES',0);

insert into cooperative_type(id,version,name) values (1,0,'Cooperativa de consum (pares i mares)');
insert into cooperative_type(id,version,name) values (2,0,'Cooperativa de treball associat');
insert into cooperative_type(id,version,name) values (3,0,'Cooperativa de  segon grau');
insert into cooperative_type(id,version,name) values (4,0,'Cooperativa integral');

insert into school_type(id,version,name) values (1,0,'Privada');
insert into school_type(id,version,name) values (2,0,'Privada Concertada');
insert into school_type(id,version,name) values (3,0,'Educació especial');
insert into school_type(id,version,name) values (4,0,'Escola bressol');

insert into course(id,version,name) values (1,0,'Educació infantil 0-1 any');
insert into course(id,version,name) values (2,0,'Educació infantil 1-2 anys');
insert into course(id,version,name) values (3,0,'Educació infantil 2-3 anys');
insert into course(id,version,name) values (4,0,'Educació infantil de P3-P5');
insert into course(id,version,name) values (5,0,'Educació Primària de 1r a 6è');
insert into course(id,version,name) values (6,0,'Educació Secundària Obligatòria (ESO)');
insert into course(id,version,name) values (7,0,'Batxillerat');
insert into course(id,version,name) values (8,0,'Educació Especial infants amb TEA');
insert into course(id,version,name) values (9,0,'Cicles formatius de 2n grau, certificats de treball i cursos de formació ocupacional');
insert into course(id,version,name) values (10,0,'Escola de música');
insert into course(id,version,name) values (11,0,'Educació Especial i Programes de TVA (trans. vida adulta)');
insert into course(id,version,name) values (12,0,'PFI. Auxiliar d''arts gràfiquesi serigrafia/ Auxiliar activitats oficina i seregrafia');

insert into job(id,version,name) values (1,0,'President/a');
insert into job(id,version,name) values (2,0,'Director/a');
insert into job(id,version,name) values (3,0,'Secretàri/a');

insert into event(id,version,name) values (1,0,'Evento de prova');

insert into school(id,version,nif,school_name,address,id_town,cp,phone,web)values (5,0,'info@waldorflafont.org','La Font','Ronda Francesc Camprodon, 2',245,'08500','695788133','www.waldorflafont.org');
insert into contact(id,version,name,email,id_position,id_school) values (1,0,'Gabriel Aguilera Baeza',null,1,5);
insert into contact(id,version,name,email,id_position,id_school) values (2,0,'Antonio Mostazo González','direccio@lesneus.cat',2,5);
insert into contact(id,version,name,email,id_position,id_school) values (3,0,'Amparo Martí','secretaria@lesneus.cat',3,5);
--insert into activity(id,version,name,description,date,duration,place,id_school) values(1,0,'Conferències, fires, tallers','Jornada de portes obertes','2019-03-16','1 dia','A l''escola',5);
--insert into knowledge_area(id,version,name,description,id_school) values (1,0,'Pedagogia Waldorf','Catalunya',5);
